package com.epam.urban.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.AuthorService;
import com.epam.urban.newsmanagement.logic.service.NewsManagementService;
import com.epam.urban.newsmanagement.model.Author;



@Controller
@RequestMapping("/admin/editAuthor")
public class AdminEditAuthorController {

	@Autowired
	private AuthorService authorService;
	@Autowired
	private NewsManagementService newsManagementService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView authorList(Model model) {

		return getData();
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveAuthor(@RequestParam("authorId") Long authorId,
			@RequestParam("name") String name) {
		Author author = new Author();
		author.setId(authorId);
		author.setName(name);
		try {
			authorService.save(author);
		} catch (LogicException e) {
			return error(e);
		}

		return new ModelAndView("redirect:/admin/editAuthor.html");
	}
	
	@RequestMapping(value = "/delete/{authorId}", method = RequestMethod.GET)
	public ModelAndView deleteAuthor(@PathVariable Long authorId) {
		
		try {
			authorService.deleteAuthor(new Long[] { authorId });
		} catch (LogicException e) {
			return error(e);
		}
		
		return new ModelAndView("redirect:/admin/editAuthor.html");
	}
	
	@RequestMapping(value = "/expire/{authorId}", method = RequestMethod.GET)
	public ModelAndView expireAuthor(@PathVariable Long authorId) {
		
		try {
			authorService.expireAuthor(authorId);
		} catch (LogicException e) {
			return error(e);
		}
		
		return new ModelAndView("redirect:/admin/editAuthor.html");
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView saveAuthor(@RequestParam("name") String name) {
		Author author = new Author();
		author.setName(name);
		try {
			authorService.addAuthor(author);
		} catch (LogicException e) {
			return error(e);
		}

		return new ModelAndView("redirect:/admin/editAuthor.html");
	}

	private ModelAndView getData() {
		ModelAndView model = new ModelAndView();
		model.setViewName("adminEditAuthor");
		List<Author> authorList = null;

		try {
			authorList = authorService.getNotExpiredAuthorList();
		} catch (LogicException e) {
			return error(e);
		}
		model.addObject("authorList", authorList);
		return model;
	}
	
	private ModelAndView error(LogicException e) {
		ModelAndView model = new ModelAndView();
		model.setViewName("errorPage");
		model.addObject("error", e);
		return model;
	}

}
