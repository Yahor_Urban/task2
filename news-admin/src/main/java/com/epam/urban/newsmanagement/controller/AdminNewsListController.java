package com.epam.urban.newsmanagement.controller;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.AuthorService;
import com.epam.urban.newsmanagement.logic.service.NewsManagementService;
import com.epam.urban.newsmanagement.logic.service.NewsService;
import com.epam.urban.newsmanagement.logic.service.TagService;
import com.epam.urban.newsmanagement.model.Author;
import com.epam.urban.newsmanagement.model.Filter;
import com.epam.urban.newsmanagement.model.NewsVO;
import com.epam.urban.newsmanagement.model.Tag;

@Controller
@SessionAttributes("filter")
public class AdminNewsListController {

	@Autowired
	private NewsService newsService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;
	@Autowired
	private NewsManagementService newsManagementService;

	@RequestMapping("/user/adminNewsList/{page}")
	public ModelAndView adminNewsList(@PathVariable int page, HttpSession httpSession) {

		Filter filter = (Filter) httpSession.getAttribute("filter");
		return getData(page, filter);
	}

	@RequestMapping("/user/adminNewsList/reset")
	public ModelAndView reset() {

		return getData(1, null);
	}

	// @RequestMapping(value = "/user/adminNewsList/getByTags", method = RequestMethod.POST)
	// public ModelAndView getNewsByAuthor(@ModelAttribute Filter filter) {
	// List<NewsVO> newsList = null;
	// List<Author> authorList = null;
	// List<Tag> tagList = null;
	//
	// try {
	// authorList = authorService.getList();
	// tagList = tagService.getTagList();
	// // newsList = newsService.getListNewsByTagAndAuthor(authorId, tagId);
	// } catch (LogicException e) {
	// return error(e);
	// }
	// // model.addAttribute("newsList", newsList);
	// // model.addAttribute("pages", Pager.getPagedListHolder().getPageCount());
	// // model.addAttribute("newsList", Pager.getPagedListHolder());
	// // model.addAttribute("tagList", tagList);
	// // model.addAttribute("authorList", authorList);
	// // return new ModelAndView("adminNewsList");
	// return getData(0, filter);
	//
	// }

	@RequestMapping(value = "/user/adminNewsList/getFiltered", method = RequestMethod.POST)
	public ModelAndView test(@ModelAttribute Filter filter) {

		return getData(1, filter);
	}

	@RequestMapping(value = "/admin/adminNewsList/delete", method = RequestMethod.POST)
	public ModelAndView deleteNews(@RequestParam(value = "newsId", required = false) Long[] newsId) {

		if (newsId != null) {
			try {
				newsManagementService.delete(newsId);
			} catch (LogicException e) {
				return error(e);
			}
		}

		return getData(1, null);
	}

	private ModelAndView getData(int page, Filter filter) {
		List<Author> authorList = null;
		List<Tag> tagList = null;
		List<NewsVO> newsList = null;
		if (filter == null) {
			filter = new Filter();
		}
		ModelAndView model = new ModelAndView();
		model.setViewName("adminNewsList");
		int count = 0;
		try {
			int newsMin = Constants.MAX_NEWS * (page - 1) + 1;
			int newsMax = page * Constants.MAX_NEWS;
			newsList = newsService.getNewsList(newsMin, newsMax, filter);

			count = newsService.getCountOfNews(filter);

			authorList = authorService.getList();
			tagList = tagService.getTagList();
		} catch (LogicException e) {
			return error(e);
		}

		model.addObject("pages", (count + (Constants.MAX_NEWS - 1)) / Constants.MAX_NEWS);
		model.addObject("newsList", newsList);

		filter.setAuthor(authorList);
		filter.setTag(tagList);
		model.addObject("filter", filter);
		List<String> authors = new LinkedList<String>();
		for (Author a : authorList) {
			authors.add(a.getName());
		}
		model.addObject("testAuthor", authors);

		return model;
	}

	private ModelAndView error(LogicException e) {
		ModelAndView model = new ModelAndView();
		model.setViewName("errorPage");
		model.addObject("error", e);
		return model;
	}
}
