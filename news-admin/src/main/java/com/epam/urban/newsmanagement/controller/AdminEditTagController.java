package com.epam.urban.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.NewsManagementService;
import com.epam.urban.newsmanagement.logic.service.TagService;
import com.epam.urban.newsmanagement.model.Tag;

@Controller
@RequestMapping("/admin/editTag")
public class AdminEditTagController {

	@Autowired
	private TagService tagService;
	@Autowired
	private NewsManagementService newsManagementService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView tagList(Model model) {

		return getData();
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveTag(@RequestParam("tagId") Long tagId,
			@RequestParam("name") String name) {
		Tag tag = new Tag();
		tag.setId(tagId);
		tag.setTagName(name);
		try {
			tagService.save(tag);
		} catch (LogicException e) {
			return error(e);
		}

		return new ModelAndView("redirect:/admin/editTag.html");
	}
	
	@RequestMapping(value = "/delete/{tagId}", method = RequestMethod.GET)
	public ModelAndView delTag(@PathVariable Long tagId) {
		
		try {
			tagService.deleteTag(tagId);
		} catch (LogicException e) {
			return error(e);
		}
		
		return new ModelAndView("redirect:/admin/editTag.html");
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addTag(@RequestParam("name") String name) {
		Tag tag = new Tag();
		tag.setTagName(name);
		try {
			tagService.addTag(tag);
		} catch (LogicException e) {
			return error(e);
		}

		return new ModelAndView("redirect:/admin/editTag.html");
	}
	
	
	private ModelAndView getData() {
		ModelAndView model = new ModelAndView();
		model.setViewName("adminEditTag");
		List<Tag> tagList = null;

		try {
			tagList = tagService.getTagList();
		} catch (LogicException e) {
			return error(e);
		}
		model.addObject("tagList", tagList);
		return model;
	}
	
	private ModelAndView error(LogicException e) {
		ModelAndView model = new ModelAndView();
		model.setViewName("errorPage");
		model.addObject("error", e);
		return model;
	}
}
