package com.epam.urban.newsmanagement.controller;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.AuthorService;
import com.epam.urban.newsmanagement.logic.service.NewsManagementService;
import com.epam.urban.newsmanagement.logic.service.NewsService;
import com.epam.urban.newsmanagement.logic.service.TagService;
import com.epam.urban.newsmanagement.model.Author;
import com.epam.urban.newsmanagement.model.News;
import com.epam.urban.newsmanagement.model.NewsVO;
import com.epam.urban.newsmanagement.model.Tag;

@Controller
@RequestMapping("/user")
public class AddNewsController {
	
	@Autowired
	private NewsService newsService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;
	@Autowired
	private NewsManagementService newsManagementService;
	
	@RequestMapping("/addNews")
	public ModelAndView addNewsCreate(Model model) {
		List<Author> authorList = null;
		List<Tag> tagList = null;
		
		try {
			authorList = authorService.getNotExpiredAuthorList();
			tagList = tagService.getTagList();
		} catch (LogicException e) {
			return error(e, model);
		}
		model.addAttribute("tagList", tagList);
		model.addAttribute("authorList", authorList);
//		model.addAttribute("date", new Timestamp(System.currentTimeMillis()));
		return new ModelAndView("addNews");
	}

	@RequestMapping(value = "/addNews", method = RequestMethod.POST)
	public ModelAndView addNews(@RequestParam("title") String title,
			@RequestParam("creationDate") String creationDate,
			@RequestParam("brief") String brief,
			@RequestParam("content") String content,
			@RequestParam("author") Long authorId, Locale locale,
			@RequestParam("tag") Long[] tagId, Model model) {
		NewsVO newsVO = new NewsVO();
		News news = new News();
		if (validate(locale, creationDate)) {
			try {
				news.setCreationDate(parseDate(creationDate, locale));
			} catch (ParseException e) {
				return error(e, model);
			}
			news.setTitle(title);
			news.setShortText(brief);
			news.setFullText(content);
			java.util.Date date = new java.util.Date();
			long t = date.getTime();
			java.sql.Date sqlDate = new java.sql.Date(t);
			news.setModificationDate(sqlDate);
			Author author = new Author();
			author.setId(authorId);
			for (int i = 0; i < tagId.length; i++) {
				Tag tag = new Tag();
				tag.setId(tagId[i]);
				newsVO.addTags(tag);
			}
			newsVO.setNews(news);
			newsVO.setAuthor(author);
			try {
				newsManagementService.add(newsVO);
			} catch (LogicException e) {
				return error(e, model);
			}
			return new ModelAndView("redirect:/user/adminViewNews/"+newsVO.getNews().getId()+".html");
		} else {
			List<Author> authorList = null;
			List<Tag> tagList = null;
			try {
				authorList = authorService.getNotExpiredAuthorList();
				tagList = tagService.getTagList();
			} catch (LogicException e) {
				return error(e, model);
			}
			model.addAttribute("tagList", tagList);
			model.addAttribute("authorList", authorList);
			model.addAttribute("msg", true);
			return new ModelAndView("addNews");
		}
//		news.setCreationDate(new Timestamp(System.currentTimeMillis()));
	}
	
	private boolean validate(Locale locale, String date) {
		Pattern pattern;
		switch (locale.toString()) {
		case "ru":
			pattern = Pattern.compile(Constants.DATE_REGEX_RU);
			break;
		default:
			pattern = Pattern.compile(Constants.DATE_REGEX_EN);
			break;
		}
		Matcher matcher = pattern.matcher(date);
		return matcher.matches();		
	}
	
	private Date parseDate(String date, Locale locale) throws ParseException {
		DateFormat formatter;
		switch (locale.toString()) {
		case "ru":
			formatter = new SimpleDateFormat(Constants.DATE_PATTERN_RU);
			break;
		default:
			formatter = new SimpleDateFormat(Constants.DATE_PATTERN_EN);
			break;
		}
		return new Timestamp(formatter.parse(date).getTime());
	}
	
	private ModelAndView error(Exception e, Model model) {
		model.addAttribute("error", e);
		return new ModelAndView("errorPage");
	}

}
