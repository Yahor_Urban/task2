package com.epam.urban.newsmanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.CommentService;
import com.epam.urban.newsmanagement.logic.service.NewsManagementService;
import com.epam.urban.newsmanagement.logic.service.NewsService;
import com.epam.urban.newsmanagement.model.Comment;
import com.epam.urban.newsmanagement.model.Filter;
import com.epam.urban.newsmanagement.model.NewsVO;

@Controller
@SessionAttributes("filter")
public class AdminSingleNewsController {

	@Autowired
	private NewsService newsService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private NewsManagementService newsManagementService;

	@RequestMapping(value = "/user/adminViewNews/{newsId}", method = RequestMethod.GET)
	public ModelAndView viewSingleNews(@PathVariable Long newsId, Model model) {

		return getData(newsId);
	}

	@RequestMapping(value = "/user/adminViewNews/addComment", method = RequestMethod.POST)
	public ModelAndView addComment(@RequestParam("id") Long newsId, @RequestParam("commentText") String commentText) {
		if (commentText != null && !commentText.trim().isEmpty()) {
			Comment comment = new Comment();
			comment.setNewsId(newsId);
			comment.setCommentText(commentText);
			try {
				commentService.add(comment);
			} catch (LogicException e) {
				return error(e);
			}
		}
		return new ModelAndView("redirect:/user/adminViewNews/" + newsId + ".html");
	}

	@RequestMapping(value = "/admin/adminViewNews/delComment/{commentId}/{newsId}", method = RequestMethod.GET)
	public ModelAndView deleteComment(@PathVariable Long commentId, @PathVariable Long newsId) {

		try {
			commentService.deleteComment(new Long[] { commentId });
		} catch (LogicException e) {
			return error(e);
		}

		return getData(newsId);
	}

	@RequestMapping(value = "/user/adminViewNews/next/{newsId}", method = RequestMethod.GET)
	public ModelAndView nextNews(@PathVariable Long newsId, @ModelAttribute Filter filter) {

		return getNextOrPreviosNews(newsId, Constants.NEXT, filter);
	}

	@RequestMapping(value = "/user/adminViewNews/previous/{newsId}", method = RequestMethod.GET)
	public ModelAndView nextPrevious(@PathVariable Long newsId, @ModelAttribute Filter filter) {

		return getNextOrPreviosNews(newsId, Constants.PREVIOUS, filter);
	}

	private ModelAndView getData(Long newsId) {
		ModelAndView model = new ModelAndView();
		model.setViewName("adminViewNews");
		NewsVO newsVO = null;

		try {
			newsVO = newsService.getSingleNewsMessage(newsId);
			newsVO.setComments(commentService.getNewsCommentList(newsId));
		} catch (LogicException e) {
			model.setViewName("errorPage");
			model.addObject("error", e);
			return model;
		}
		model.addObject("newsVO", newsVO);
		return model;
	}

	private ModelAndView getNextOrPreviosNews(Long newsId, String type, Filter filter) {
		ModelAndView model = new ModelAndView();
		model.setViewName("adminViewNews");
		NewsVO newsVO = null;

		try {
			switch (type) {
			case Constants.NEXT:
				newsVO = newsService.getNextNews(newsId, filter);
				break;
			case Constants.PREVIOUS:
				newsVO = newsService.getPreviousNews(newsId, filter);
				break;
			default:
				break;
			}
			if (newsVO != null) {
				newsVO.setComments(commentService.getNewsCommentList(newsVO.getNews().getId()));
			}
		} catch (LogicException e) {
			error(e);
		}
		model.addObject("newsVO", newsVO);

		return model;
	}

	private ModelAndView error(LogicException e) {
		ModelAndView model = new ModelAndView();
		model.setViewName("errorPage");
		model.addObject("error", e);
		return model;
	}
}
