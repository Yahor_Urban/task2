package com.epam.urban.newsmanagement.controller;



import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.AuthorService;
import com.epam.urban.newsmanagement.logic.service.NewsManagementService;
import com.epam.urban.newsmanagement.logic.service.NewsService;
import com.epam.urban.newsmanagement.logic.service.TagService;
import com.epam.urban.newsmanagement.model.Filter;
import com.epam.urban.newsmanagement.model.News;
import com.epam.urban.newsmanagement.model.NewsVO;
import com.epam.urban.newsmanagement.model.Tag;

@Controller
@RequestMapping("/admin")
@SessionAttributes("filter1")
public class AdminEditNewsController {

	@Autowired
	private NewsService newsService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;
	@Autowired
	private NewsManagementService newsManagementService;	

	@RequestMapping(value = "/editNews/{newsId}", method = RequestMethod.GET)
	public ModelAndView viewSingleNews(@PathVariable Long newsId, Model model) {
		NewsVO newsVO = null;
		Filter filter1 = null;
		List<Long> checkedTags = new ArrayList<>();
		if (filter1 == null) {
			filter1 = new Filter();
			try {
				filter1.setAuthor(authorService.getNotExpiredAuthorList());
				filter1.setTag(tagService.getTagList());
			} catch (LogicException e) {
				return error(e);
			}
		}
		try {
			newsVO = newsManagementService.getSingleNews(newsId);
			filter1.setCheckedAuthor(newsVO.getAuthor().getId());
			for (Tag tag : newsVO.getTags()) {
				checkedTags.add(tag.getId());
			}
			filter1.setCheckedTags(checkedTags.toArray(new Long[checkedTags.size()]));
		} catch (LogicException e) {
			model.addAttribute("error", e);
			return new ModelAndView("errorPage");
		}
		model.addAttribute("filter1", filter1);
		model.addAttribute("newsVO", newsVO);
		return new ModelAndView("adminEditNews");
	}

	@RequestMapping(value="/editNews", method = RequestMethod.POST)
	public ModelAndView saveSingleNews(@RequestParam("title") String title,
			@RequestParam("brief") String brief,
			@RequestParam("newsId") Long newsId,
			@RequestParam("date") String mDateOld,
			@RequestParam("modification") String modificationDate,
			@RequestParam("content") String content, Locale locale,
			@ModelAttribute Filter filter1, Model model) {
		News news = new News();
		news.setId(newsId);
		news.setTitle(title);
		news.setShortText(brief);
		news.setFullText(content);
		if (validate(locale, modificationDate)) {
			try {
				news.setModificationDate(parseDate(modificationDate, locale));
			} catch (ParseException e) {
				return error(e);
			}
		} else {
			model.addAttribute("msg", true);
			NewsVO newsVO = new NewsVO();
			try {
				news.setModificationDate(parseDate(mDateOld, locale));
			} catch (ParseException e) {
				return error(e);
			}
			newsVO.setNews(news);
			model.addAttribute("newsVO", newsVO);
			return new ModelAndView("adminEditNews");
		}
		try {
			newsManagementService.updateNews(news, filter1);
		} catch (LogicException e) {
			return error(e);
		}
//		model.addAttribute("filter", new Filter());

		return new ModelAndView("redirect:/user/adminNewsList/1.html");
	}
	
	private ModelAndView error(Exception e) {
		ModelAndView model = new ModelAndView();
		model.setViewName("errorPage");
		model.addObject("error", e);
		return model;
	}
	
	private boolean validate(Locale locale, String date) {
		Pattern pattern;
		switch (locale.toString()) {
		case "ru":
			pattern = Pattern.compile(Constants.DATE_REGEX_RU);
			break;
		default:
			pattern = Pattern.compile(Constants.DATE_REGEX_EN);
			break;
		}
		Matcher matcher = pattern.matcher(date);
		return matcher.matches();		
	}
	
	private Date parseDate(String date, Locale locale) throws ParseException {
		DateFormat formatter;
		switch (locale.toString()) {
		case "ru":
			formatter = new SimpleDateFormat(Constants.DATE_PATTERN_RU);
			break;
		default:
			formatter = new SimpleDateFormat(Constants.DATE_PATTERN_EN);
			break;
		}
		return new java.sql.Date(formatter.parse(date).getTime());
	}

}
