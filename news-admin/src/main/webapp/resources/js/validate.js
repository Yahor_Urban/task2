function validate() {
	var title = document.forms["form"]["title"].value;
	var brief = document.forms["form"]["brief"].value;
	var content = document.forms["form"]["content"].value;

	$(".form-control").css({
		"background-color" : "#FFFFFF"
	});
	if (title.length == 0 || brief.length == 0 || content.length == 0) {
		if (title.length == 0) {
			$("#t").css({
				"background-color" : "#f2dede"
			});
		}
		if (brief.length == 0) {
			$("#b").css({
				"background-color" : "#f2dede"
			});
		}
		if (content.length == 0) {
			$("#c").css({
				"background-color" : "#f2dede"
			});
		}
		return false;
	} else {
		return true;
	}
}

function validateEditNews() {
	var title = document.forms["form"]["title"].value;
	var brief = document.forms["form"]["brief"].value;
	var content = document.forms["form"]["content"].value;
	var modification = document.forms["form"]["modification"].value;
	$(".form-control").css({
		"background-color" : "#FFFFFF"
	});
	if (title.length == 0 || brief.length == 0 || content.length == 0) {
		if (title.length == 0) {
			$("#t").css({
				"background-color" : "#f2dede"
			});
		}
		if (brief.length == 0) {
			$("#b").css({
				"background-color" : "#f2dede"
			});
		}
		if (content.length == 0) {
			$("#c").css({
				"background-color" : "#f2dede"
			});
		}
		if (modification.length == 0) {
			$("#m").css({
				"background-color" : "#f2dede"
			});
		}
		return false;
	} else {
		return true;
	}
}

//function validateEdit() {
//	var edit = document.forms["form"]["name"].value;
//	var e = $("#disabledTextInput").value;
//	$(".form-control").css({
//		"background-color" : "#FFFFFF"
//	});
//	if (edit.length == 0) {
//		$("#disabledTextInput").css({
//			"background-color" : "#f2dede"
//		});
//		return false;
//	} else {
//		return true;
//	}
//
//}