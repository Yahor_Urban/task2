function test($i) {
	$("#disabledTextInput" + $i).prop('disabled', false);
	$("#edit" + $i).hide();
	$("#save" + $i).show();
	$("#delete" + $i).show();
	$("#expire" + $i).show();
	$(this).toggleClass("active");
}

function reset() {
	$(".checkbox").checked = true;
}