<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


<form:form class="form-inline" method="post" action="getFiltered.html"
	commandName="filter" style="text-align: center;">
	<div class="form-group">
		<form:select path="checkedAuthor">
			<form:option value="" label="--- Author ---" />
			<form:options items="${filter.author}" itemLabel="name"
				itemValue="id" />
		</form:select>
	</div>
	<div class="form-group">
		<div class="select">
			<span><spring:message code="label.tag" /></span>
			<div class="select_">
				<form:checkboxes id="s1" items="${filter.tag}" path="checkedTags" 
				itemLabel="tagName" itemValue="id" element="span class='checkbox'"/>
			</div>
		</div>
	</div>
	
	<input type="submit" value="<spring:message code="label.filter"/>">
	<a href="reset.html" style="color: black;"><button type="button">
			<spring:message code="label.reset" />
		</button></a>
</form:form>

<form method="post" action="/news-admin/admin/adminNewsList/delete.html">
	<c:forEach items="${newsList}" var="newsVO">
		<div class="row">
			<div class="col-md-8">${newsVO.news.title}</div>
			<div class="col-md-2">
				<p class="text-right">(by ${newsVO.author.name})</p>
			</div>
			<div class="col-md-2">
				<p class="text-right">
					<fmt:formatDate value="${newsVO.news.creationDate}" />
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">${newsVO.news.shortText}</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-8">
				<p class="text-right" style="color: #696969;">
					<c:forEach items="${newsVO.tags}" var="tag">
							${tag.tagName}
					</c:forEach>
				</p>
			</div>
			<div class="col-md-2">
				<p class="text-right">
					<a href="/news-admin/user/adminViewNews/${newsVO.news.id}.html"
						style="color: #FF4500;"><spring:message code="label.comments" />(${newsVO.countOfComments})</a>
				</p>
			</div>
			<div class="col-md-2">
				<p class="text-right">
					<a href="/news-admin/admin/editNews/${newsVO.news.id}.html"> <spring:message
							code="label.edit" />
					</a> <input type="checkbox" name="newsId" value="${newsVO.news.id}" />
				</p>
			</div>
		</div>
		<br />
	</c:forEach>
	<div align="right">
		<button type="button" data-toggle="modal"
			data-target=".bs-example-modal-sm"><spring:message code="label.delete"/></button>
		<div class="modal fade bs-example-modal-sm" tabindex="-1"
			role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="gridSystemModalLabel">Уверены?</h4>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<input type="submit" class="btn btn-default" value="<spring:message code="label.delete"/>" />
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<c:if test="${not empty pages}">
	<div class="row">
		<div class="col-md-12"
			style="text-align: center; background: #f0f0f0;">
			<c:forEach var="i" begin="1" end="${pages}">
				<a href="/news-admin/user/adminNewsList/${i}.html"
					style="color: black;"><input type="button" value="${i}"></a>
			</c:forEach>
		</div>
	</div>
</c:if>

