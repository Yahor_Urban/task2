<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="now" class="java.util.Date"/>
<script type="text/javascript" src="/news-admin/resources/js/validate.js"></script>
<form method="POST" action="addNews.html" class="form-horizontal" name="form" onsubmit="return validate()">

	<div class="form-group">
		<label class="col-sm-1 control-label" for="f"><spring:message
				code="label.title" /> </label>
		<div class="col-sm-10">
			<input class="form-control" type="text" name="title" id="t"  maxlength="20"/>
		</div>
	</div>

		<div class="form-group">
			<label class="col-sm-1 control-label" for="disabledTextInput"><spring:message
					code="label.date" /> </label>
			<div class="col-sm-3">
<!-- 				<input class="form-control" type="text" name="date" size="52" -->
<%-- 					value="<fmt:formatDate value="${date}"/>" id="disabledTextInput" /> --%>
					<input class="form-control" type="text" name="creationDate" size="52"
					value="<fmt:formatDate value="${now}"/>" id="disabledTextInput" />
			</div>
		</div>

	<div class="form-group">
		<label class="col-sm-1 control-label" for="b"><spring:message
				code="label.brief" /> </label>
		<div class="col-sm-10">
			<textarea class="form-control" name="brief" rows="2" cols="40" id="b" maxlength="100"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-1 control-label" for="c"><spring:message
				code="label.content" /> </label>
		<div class="col-sm-10">
			<textarea class="form-control" name="content" rows="5" cols="40"
				id="c" maxlength="2000"></textarea>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-3"></div>
		<div class="col-xs-2">
			<select name="author">
				<c:forEach items="${authorList}" var="a">
					<option value="${a.id}">${a.name}</option>
				</c:forEach>
			</select>
		</div>
		<div class="col-xs-1">
			<div class="select">
				<span><spring:message code="label.tag" /></span>
				<div class="select_">
					<c:forEach items="${tagList}" var="t">
						<div>
							<input type="checkbox" name="tag" value="${t.id}" checked>
							${t.tagName}
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${msg}">
		<div class="row">
			<div class="col-sm-offset-4 col-sm-3">
				<div class="msg">
					<spring:message code="label.incorrect_date" />
				</div>
			</div>
		</div>
	</c:if>
	<div class="row">
		<div class="col-sm-11" align="right">
			<input type="submit" value="<spring:message code="label.save"/>" />
		</div>
	</div>
</form>