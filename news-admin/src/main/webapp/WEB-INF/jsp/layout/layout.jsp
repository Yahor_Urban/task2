<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

<title><tiles:insertAttribute name="title" ignore="true" /></title>
</head>
<link rel="stylesheet" type="text/css"
	href="/news-admin/resources/css/bootstrap.css">
<body>
	<script type="text/javascript"
		src="/news-admin/resources/js/jquery-1.11.2.js"></script>
	<script type="text/javascript" src="/news-admin/resources/js/s.js"></script>
	<!-- 	<script type="text/javascript" src="/news-admin/resources/js/validate.js"></script> -->
	<script type="text/javascript"
		src="/news-admin/resources/js/bootstrap.min.js"></script>

	<div class="row" style="outline: 2px solid #000;">
		<div class="col-md-12" style="outline: 2px solid #000;">
			<tiles:insertAttribute name="header" />
		</div>
		<div class="col-xs-12">
			<div class="col-xs-2">
				<tiles:insertAttribute name="sidebar" />
			</div>
			<div class="col-xs-10" style="border: 1px solid #000;">
				<tiles:insertAttribute name="body" />
			</div>
		</div>
		<div class="col-md-12" style="outline: 2px solid #000;">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>
</body>
</html>