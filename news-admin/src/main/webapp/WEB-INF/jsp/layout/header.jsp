<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<sec:authorize access="isAuthenticated()">
	<sec:authentication property="principal.username" var="username" />
</sec:authorize>
<div class="row">
	<div class="col-md-4">
		<h1>
			<spring:message code="label.main_title" />
		</h1>
	</div>
	<div class="col-md-2 col-md-offset-5">
		<div style="float: right">
			<spring:message code="label.hello"/>
			<c:choose>
				<c:when test="${not empty username}">${username}</c:when>
				<c:otherwise>Anon</c:otherwise>
			</c:choose>
		</div>
	</div>
	
	<div class="col-md-1">
		<a href="<c:url value="/j_spring_security_logout"/>" style="color: black;">
			<input type="button" value="<spring:message code="label.logout"/>">
		</a>	
	</div>
</div>
<div class="row">
	<div class="col-md-1 col-md-offset-11">
		<span style="float: right"> <a href="?lang=en">EN</a> <a
			href="?lang=ru">RU</a>
		</span>
	</div>
</div>