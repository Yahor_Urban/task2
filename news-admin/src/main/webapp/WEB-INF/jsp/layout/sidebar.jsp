<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<a href="/news-admin/user/adminNewsList/1.html"><spring:message code="label.news_list"/></a>
<br />
<a href="/news-admin/user/addNews.html"><spring:message code="label.add_news"/></a>
<br />
<a href="/news-admin/admin/editAuthor.html"><spring:message code="label.edit_author"/></a>
<br />
<a href="/news-admin/admin/editTag.html"><spring:message code="label.edit_tag"/></a>
<br />
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Launch demo modal
</button>


<!-- Modal -->
<jsp:include page="../test/addNewsTest.jsp" />

