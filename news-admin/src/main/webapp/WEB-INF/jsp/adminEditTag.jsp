<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src="/news-admin/resources/js/validate.js"></script>
<c:forEach var="i" begin="0" end="${tagList.size()-1}">
	<div class="row">
		<form id="form" method="POST"
			action="/news-admin/admin/editTag/save.html">
			<div class="col-md-10">
				<fieldset>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="disabledTextInput">Tag:</label>
						<div class="col-sm-10">
							<input class="form-control" type="text" name="name" size="50" maxlength="30"
								value="${tagList.get(i).tagName}" id="disabledTextInput${i}"
								disabled="true" />
						</div>
					</div>
				</fieldset>
			</div>
			<div class="col-md-1">
				<button id="edit${i}" type="button" onclick=test(${i});>
					<spring:message code="label.edit" />
				</button>

				<input type="hidden" value="${tagList.get(i).id}" name="tagId" /> <input
					id="save${i}" type="submit"
					value="<spring:message code="label.save"/>" hidden>

			</div>
			<div class="col-md-1 ">
				<a id="delete${i}"
					href="/news-admin/admin/editTag/delete/${tagList.get(i).id}.html" hidden
					style="color: black;"><input type="button" value="<spring:message code="label.delete"/>"></a>
			</div>
		</form>
	</div>
</c:forEach>
<div class="row">
	<form method="POST" action="/news-admin/admin/editTag/add.html" name="form" onsubmit="return validateEdit()">
		<div class="col-md-10">
			<fieldset>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="disabledTextInput">Tag:</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="name" size="50" maxlength="30"
							id="disabledTextInput" />
					</div>
				</div>
			</fieldset>
		</div>
		<div class="col-md-2">
			<input type="submit" value="<spring:message code="label.save"/>">
		</div>
	</form>
</div>