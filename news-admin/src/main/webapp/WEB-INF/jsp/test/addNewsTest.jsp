<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:useBean id="now" class="java.util.Date" />

<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="/news-admin/user/addNews.html"
				class="form-horizontal" name="formT" onsubmit="return validate()">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">
						<spring:message code="label.add_news" />
					</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-sm-1 control-label" for="f"><spring:message
								code="label.title" /> </label>
						<div class="col-sm-10 col-sm-offset-1">
							<input class="form-control" type="text" name="title" id="tT"
								aria-describedby="inputWarning2Status" />
						</div>
					</div>
					<fieldset disabled>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="disabledTextInput"><spring:message
									code="label.date" /> </label>
							<div class="col-sm-3 col-sm-offset-1">
								<!-- 				<input class="form-control" type="text" name="date" size="52" -->
								<%-- 					value="<fmt:formatDate value="${date}"/>" id="disabledTextInput" /> --%>
								<input class="form-control" type="text" name="date" size="52"
									value="<fmt:formatDate value="${now}"/>" id="disabledTextInput" />
							</div>
						</div>
					</fieldset>
					<div class="form-group">
						<label class="col-sm-1 control-label" for="b"><spring:message
								code="label.brief" /> </label>
						<div class="col-sm-10 col-sm-offset-1">
							<textarea class="form-control" name="brief" rows="2" cols="40"
								id="bT"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-1 control-label" for="c"><spring:message
								code="label.content" /> </label>
						<div class="col-sm-10 col-sm-offset-1">
							<textarea class="form-control" name="content" rows="5" cols="40"
								id="cT"></textarea>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-1 col-sm-offset-4">
							<select name="author">
								<c:forEach items="${authorList}" var="a">
									<option value="${a.id}">${a.name}</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-sm-1 col-sm-offset-1">
							<div class="select">
								<span><spring:message code="label.tag" /></span>
								<div class="select_">
									<c:forEach items="${tagList}" var="t">
										<div>
											<input type="checkbox" name="tag" value="${t.id}" checked>
											${t.tagName}
										</div>
									</c:forEach>
								</div>
							</div>
						</div>
					</div>
				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">
						<spring:message code="label.save" />
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
