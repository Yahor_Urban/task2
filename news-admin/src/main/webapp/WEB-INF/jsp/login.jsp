<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="true" isELIgnored="false"%>


<html>

<head>
<link rel="stylesheet" type="text/css" href="/news-admin/resources/css/bootstrap.css">

<title>Login</title>

</head>

<body onload='document.loginForm.username.focus();'>
	<div id="login-box">
		<form class="form-horizontal"
			action="<c:url value='j_spring_security_check' />" method='POST'>
			<div class="form-group">
				<label for="inputLogin" class="col-sm-5 control-label">Login:</label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="inputLogin"
						name="username">
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-5 control-label">Password:</label>
				<div class="col-sm-3">
					<input type="password" class="form-control" id="inputPassword3"
						name="password">
				</div>
			</div>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<div class="row">
				<div class="col-sm-offset-5 col-sm-3">
					<c:if test="${not empty error}">
						<div class="error">${error}</div>
					</c:if>
					<c:if test="${not empty msg}">
						<div class="msg">${msg}</div>
					</c:if>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-7 col-sm-2">
					<a href="/news-client/newsList/1.html" style="color: black;">Guest</a>
					<button type="submit" name="submit">Login</button>
				</div>

			</div>
		</form>
	</div>
</body>
</html>