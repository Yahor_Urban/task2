<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript"
	src="/news-admin/resources/js/validate.js"></script>

<form:form name="form" method="POST" action="/news-admin/admin/editNews.html" commandName="filter1"
	class="form-horizontal" onsubmit="return validateEditNews()">

	<input type="hidden" name="newsId" value="${newsVO.news.id}" /> <input
		type="hidden" name="date"
		value="<fmt:formatDate value="${newsVO.news.modificationDate}"/>" />
	<div class="form-group">
		<label class="col-sm-1 control-label" for="f"><spring:message
				code="label.title" /> </label>
		<div class="col-sm-10">
			<input class="form-control" type="text" size="52" name="title"
				value="${newsVO.news.title}" id="t" maxlength="20" />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-1 control-label" for="b"><spring:message
				code="label.brief" /> </label>
		<div class="col-sm-10">
			<textarea class="form-control" name="brief" rows="2" cols="40" id="b"
				maxlength="100">${newsVO.news.shortText}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-1 control-label" for="c"><spring:message
				code="label.content" /> </label>
		<div class="col-sm-10">
			<textarea class="form-control" name="content" rows="5" cols="40"
				id="c" maxlength="2000">${newsVO.news.fullText}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-1 control-label" for="m"><spring:message
				code="label.modification_date" /> </label>
		<div class="col-sm-10">
			<input class="form-control" type="text" size="52" name="modification"
				value="<fmt:formatDate value="${newsVO.news.modificationDate}"/>"
				id="m" />
		</div>
	</div>
	<c:if test="${msg}">
		<div class="row">
			<div class="col-sm-offset-5 col-sm-2">
				<div class="msg">
					<spring:message code="label.incorrect_date" />
				</div>
			</div>
		</div>
	</c:if>

	<div class="form-group">
		<form:select path="checkedAuthor">
			<form:option value="" label="--- Author ---" />
			<form:options items="${filter1.author}" itemLabel="name"
				itemValue="id" />
		</form:select>
	</div>
	<div class="form-group">
		<div class="select">
			<span><spring:message code="label.tag" /></span>
			<div class="select_">
				<form:checkboxes items="${filter1.tag}" path="checkedTags"
					itemLabel="tagName" itemValue="id" />
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-11" align="right">
			<input type="submit" value="<spring:message code="label.save"/>" />
		</div>
	</div>
</form:form>