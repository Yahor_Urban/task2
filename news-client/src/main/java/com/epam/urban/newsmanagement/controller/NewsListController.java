package com.epam.urban.newsmanagement.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.AuthorService;
import com.epam.urban.newsmanagement.logic.service.NewsService;
import com.epam.urban.newsmanagement.logic.service.TagService;
import com.epam.urban.newsmanagement.model.Author;
import com.epam.urban.newsmanagement.model.Filter;
import com.epam.urban.newsmanagement.model.NewsVO;
import com.epam.urban.newsmanagement.model.Tag;

@Controller
@RequestMapping("/newsList")
@SessionAttributes("filter")
public class NewsListController {

	@Autowired
	private NewsService newsService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;

	@RequestMapping("/{page}")
	public ModelAndView newsList(@PathVariable int page, HttpSession httpSession) {

		Filter filter = (Filter) httpSession.getAttribute("filter");
		return getData(page, filter);
	}

	@RequestMapping(value = "/getFiltered", method = RequestMethod.POST)
	public ModelAndView getNewsByAuthor(@ModelAttribute Filter filter) {

		return getData(1, filter);
	}
	
	@RequestMapping("/reset")
	public ModelAndView reset() {

		return getData(1, null);
	}

	private ModelAndView getData(int page, Filter filter) {
		List<NewsVO> newsList = null;
		List<Author> authorList = null;
		List<Tag> tagList = null;
		if (filter == null) {
			filter = new Filter();
		}
		ModelAndView model = new ModelAndView();
		model.setViewName("newsList");
		int count = 0;
		try {
			int newsMin = Constants.MAX_NEWS * (page - 1) + 1;
			int newsMax = page * Constants.MAX_NEWS;
			newsList = newsService.getNewsList(newsMin, newsMax, filter);
			count = newsService.getCountOfNews(filter);
			authorList = authorService.getList();
			tagList = tagService.getTagList();
		} catch (LogicException e) {
			error(e);
		}
		filter.setAuthor(authorList);
		filter.setTag(tagList);
		model.addObject("filter", filter);
		model.addObject("pages", (count + (Constants.MAX_NEWS - 1)) / Constants.MAX_NEWS);
		model.addObject("newsList", newsList);
		
		return model;
	}

	private ModelAndView error(LogicException e) {
		ModelAndView model = new ModelAndView();
		model.setViewName("errorPage");
		model.addObject("error", e);
		return model;
	}

}
