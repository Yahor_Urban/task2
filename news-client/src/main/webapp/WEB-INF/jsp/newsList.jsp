<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="../resources/js/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../resources/js/s.js"></script>

<form:form class="form-inline" method="post" action="getFiltered.html" commandName="filter" style="text-align: center;">
	<div class="form-group">
		<form:select path="checkedAuthor">
			<form:option value="" label="--- Author ---" />
			<form:options items="${filter.author}" itemLabel="name" itemValue="id"/>
		</form:select>
	</div>
	<div class="form-group">
		<div class="select">
			<span><spring:message code="label.tag" /></span>
			<div class="select_">
				<form:checkboxes items="${filter.tag}" 
				path="checkedTags" itemLabel="tagName" itemValue="id" element="span class='checkbox'"/>
			</div>
		</div>
	</div>
	<input type="submit" value="<spring:message code="label.filter"/>">
	<a href="reset.html" style="color: black;"><button type="button">
			<spring:message code="label.reset" />
		</button></a>
</form:form>

	<c:forEach items="${newsList}" var="newsVO">
		<div class="row">
			<div class="col-md-8">
				${newsVO.news.title}
			</div>
			<div class="col-md-2">
				(by ${newsVO.author.name}) 
			</div>
			<div class="col-md-2">
				<fmt:formatDate value="${newsVO.news.creationDate}"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				${newsVO.news.shortText}
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-10">
				<p class="text-right" style="color: #696969;">
					<c:forEach items="${newsVO.tags}" var="tag">
							${tag.tagName}
					</c:forEach>
				</p>
			</div>
			<div class="col-md-2">
				<p class="text-right">
					<a href="/news-client/viewNews/${newsVO.news.id}.html" style="color: #FF4500;"><spring:message code="label.comments"/>(${newsVO.countOfComments})</a>
				</p>
			</div>
			
		</div>
		<br />
	</c:forEach>
	
<c:if test="${not empty pages}">
<div class="row">
	<div class="col-md-12" style="text-align: center; background: #f0f0f0;">
		<c:forEach var="i" begin="1" end="${pages}">
			<a href="/news-client/newsList/${i}.html" style="color: black;"><input type="button" value="${i}"></a>
		</c:forEach>
	</div>
</div>
</c:if>