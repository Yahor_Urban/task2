<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div class="row">
	<div class="col-md-2">
		<a href="/news-client/newsList/1.html"><spring:message
				code="label.main" /></a>
	</div>
</div>
<div class="row">
	<div class="col-md-8">${newsVO.news.shortText}</div>
	<div class="col-md-2">(by ${newsVO.author.name})</div>
	<div class="col-md-2">
		<fmt:formatDate value="${newsVO.news.creationDate}" />
	</div>
</div>
<div class="row">
	<div class="col-xs-12">${newsVO.news.fullText}</div>
	<div class="col-md-6">
		<c:forEach items="${newsVO.comments}" var="comment">
			<fmt:formatDate value="${comment.creationDate}" />
			<br />
			<div class="comment">${comment.commentText}</div>
			<br />
		</c:forEach>
		<form method="POST" action="addComment.html">

			<input type="hidden" name="id" value="${newsVO.news.id}">
			<textarea name="commentText" rows="3" cols="84"></textarea>
			<br /> <br />
			<div align="right">
				<input type="submit"
					value="<spring:message code="label.post_comment"/>" />
			</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-1">
		<a href="/news-client/viewNews/previous/${newsVO.news.id}.html"><spring:message
				code="label.prev" /></a>
	</div>
	<div class="col-md-1 col-md-offset-10">
		<a href="/news-client/viewNews/next/${newsVO.news.id}.html"><spring:message
				code="label.next" /></a>
	</div>
</div>
