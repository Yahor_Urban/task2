package com.epam.urban.newsmanagement.test.dbunit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.urban.newsmanagement.dao.TagDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:test-beans.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:data/tag/DataSetTag.xml")
public class TagDaoTest {

	@Autowired
	private TagDao tagDao;
	
	@Test
	@ExpectedDatabase(value = "classpath:data/tag/ExpectedDataSetAddTag.xml", assertionMode=DatabaseAssertionMode.NON_STRICT)
	public void addTagTest() throws Exception {
		Tag tag = new Tag();
		tag.setTagName("tag3");
		tag.setNewsId(3l);
		tagDao.addTag(tag);
//		tagDao.linkTag(3l, 3l);
	}
	
	@Test
	@ExpectedDatabase(value = "classpath:data/tag/ExpectedDataSetLinkTag.xml", assertionMode=DatabaseAssertionMode.NON_STRICT)
	public void linkTagTest() throws DAOException {
		tagDao.linkTag(3l, 3l);
	}
	
}
