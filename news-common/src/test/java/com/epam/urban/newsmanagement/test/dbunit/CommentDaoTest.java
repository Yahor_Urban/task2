package com.epam.urban.newsmanagement.test.dbunit;

import java.sql.Timestamp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.urban.newsmanagement.dao.CommentDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:test-beans.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:data/comment/DataSetComment.xml")
public class CommentDaoTest {

	@Autowired
	private CommentDao commentDao;
	
	@Test
	@ExpectedDatabase(value = "classpath:data/comment/ExpectedDataSetAddComment.xml", assertionMode=DatabaseAssertionMode.NON_STRICT)
	public void addCommentTest() throws Exception {
		Comment comment = new Comment();
		comment.setCommentText("comment3");
		comment.setCreationDate(new Timestamp(System.currentTimeMillis()));
		comment.setNewsId(2l);
		comment.setId(3l);
		commentDao.addComment(comment);
	}
	
	@Test
	@ExpectedDatabase(value = "classpath:data/comment/ExpectedDataSetSaveComment.xml", assertionMode=DatabaseAssertionMode.NON_STRICT)
	public void saveCommentTest() throws DAOException {
		Comment comment = new Comment();
		comment.setId(1l);
		comment.setCommentText("new_comment");
		commentDao.saveComment(comment);
	}
	
}
