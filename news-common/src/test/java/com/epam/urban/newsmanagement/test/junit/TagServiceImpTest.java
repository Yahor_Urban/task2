package com.epam.urban.newsmanagement.test.junit;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.urban.newsmanagement.dao.TagDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.imp.TagServiceImp;
import com.epam.urban.newsmanagement.model.Tag;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImpTest {
	
	@InjectMocks
	private TagServiceImp tagServise;
	
	@Mock
	private TagDao tagDaoMock;

	@Test
	public void test() throws DAOException, LogicException {
		Tag tag = new Tag();
		tag.setId(1l);
		tag.setTagName("name");
		tag.setNewsId(1l);
		
		when(tagDaoMock.getTagId(tag.getTagName())).thenReturn(0l);
				
		assertEquals(true, tagServise.addTag(tag));
		assertEquals(true, tagServise.save(tag));
		assertEquals(true, tagServise.deleteTag(tag.getId()));
		
		verify(tagDaoMock).getTagId("name");
		verify(tagDaoMock).addTag(tag);
		verify(tagDaoMock).saveTag(tag);
		verify(tagDaoMock).deleteTagById(tag.getId());
		verify(tagDaoMock).unlinkTagAndNews(tag.getId());
	}
}
