package com.epam.urban.newsmanagement.test.dbunit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.urban.newsmanagement.dao.NewsDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.News;
import com.epam.urban.newsmanagement.model.NewsVO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-beans.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:data/news/DataSetNews.xml")
public class NewDaoTest {

	@Autowired
	private NewsDao newsDao;

	@Test
	@ExpectedDatabase(value = "classpath:data/news/ExpectedDataSetAddNews.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void addNewsTest() throws SQLException, Exception {
		News news = new News();
		
		news.setId(3l);
		news.setShortText("short3");
		news.setFullText("fulltext3");
		news.setTitle("title3");
		news.setCreationDate(new Timestamp(System.currentTimeMillis()));
		java.util.Date date = new java.util.Date();
		long t = date.getTime();
		java.sql.Date sqlDate = new java.sql.Date(t);
		news.setModificationDate(sqlDate);
		newsDao.addNews(news);

	}

	@Test
	@ExpectedDatabase(value = "classpath:data/news/ExpectedDataSetSaveNews.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	public void saveNewsTest() throws DAOException {
		News news = new News();
		news.setId(2l);
		news.setShortText("shortnew");
		news.setFullText("fulltextnew");
		news.setTitle("titlenew");
		newsDao.saveNews(news);
	}

	@Test
	public void getNewsTest() throws DAOException {
		List<NewsVO> newsList = null;
		NewsVO newsExpected = null;

		newsList = newsDao.newsList(1, 3, null);
		newsExpected = newsDao.getNewsById(1l);
		int c = newsDao.getCountOfComments(1l);

		assertNotNull("newsList is null ", newsList);
		assertEquals("short1", newsExpected.getNews().getShortText());
		assertEquals(2, c);
	}

}
