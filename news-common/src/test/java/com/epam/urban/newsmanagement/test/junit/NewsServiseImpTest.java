package com.epam.urban.newsmanagement.test.junit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.urban.newsmanagement.dao.AuthorDao;
import com.epam.urban.newsmanagement.dao.NewsDao;
import com.epam.urban.newsmanagement.dao.TagDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.imp.NewsServiceImp;
import com.epam.urban.newsmanagement.model.News;
import com.epam.urban.newsmanagement.model.NewsVO;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiseImpTest {

	@InjectMocks
	private NewsServiceImp newsService;
	
	@Mock
	private NewsDao newsDaoMock;
	
	@Mock
	private AuthorDao authorDaoMock;
	
	@Mock
	private TagDao tagDaoMock;
	
	@Test
	public void test() throws DAOException, LogicException {
		NewsVO newsVO = new NewsVO();
		NewsVO newsVO1 = new NewsVO();
		News news = new News();
		News news1 = new News();
		news.setFullText("fulltext");
		news.setShortText("shortText");
		news.setId((long) 1);
		news1.setId((long) 2);
		newsVO.setNews(news);
		newsVO1.setNews(news1);
		newsVO.setCountOfComments(1);
		newsVO1.setCountOfComments(2);
				
		List<News> newsList = new ArrayList<News>();
		newsList.add(news1);
		newsList.add(news);
				
		when(newsDaoMock.addNews(news)).thenReturn(true);
		when(newsDaoMock.saveNews(news)).thenReturn(true);
		when(newsDaoMock.deleteNews(news.getId())).thenReturn(true);

		when(tagDaoMock.getTagId("tag")).thenReturn(1l);
		when(newsDaoMock.getNewsByTag(1l)).thenReturn(newsList);
		
		assertEquals(true, newsService.add(news));
		assertEquals(true, newsService.save(news));
		assertEquals(true, newsService.deleteNews((long) 1));
		assertEquals(newsList, newsService.getNewsByTag("tag"));

		verify(newsDaoMock).addNews(news);
		verify(newsDaoMock).saveNews(news);
		verify(newsDaoMock).deleteNews(news.getId());
		verify(tagDaoMock).getTagId("tag");
		verify(newsDaoMock).getNewsByTag(1l);
		 
	}
}
