package com.epam.urban.newsmanagement.test.dbunit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.urban.newsmanagement.dao.AuthorDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:test-beans.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:data/author/DataSetAuthor.xml")
public class AuthorDaoTest {

	@Autowired
	private AuthorDao authorDao;
	
	@Test
	@ExpectedDatabase(value = "classpath:data/author/ExpectedDataSetAddAuthor.xml", assertionMode=DatabaseAssertionMode.NON_STRICT)
	public void testAuthorAdd() throws Exception {
		Author author1 = new Author();
		author1.setName("Mariya");
		authorDao.addAuthor(author1);
	}
	
	@Test
	@ExpectedDatabase(value = "classpath:data/author/ExpectedDataSetSaveAuthor.xml", assertionMode=DatabaseAssertionMode.NON_STRICT)
	public void saveAuthorTest() throws DAOException {
		Author author = new Author();
		author.setId(1l);
		author.setName("Sasha");
		authorDao.saveAuthor(author);		
	}
	
	@Test
	@ExpectedDatabase(value = "classpath:data/author/ExpectedDataSetDeleteAuthor.xml", assertionMode=DatabaseAssertionMode.NON_STRICT)
	public void deleteAuthorTest() throws DAOException {
		authorDao.deleteAuthor(2l);
	}
	
	@Test
	@ExpectedDatabase(value = "classpath:data/author/ExpectedDataSetLinkAuthor.xml", assertionMode=DatabaseAssertionMode.NON_STRICT)
	public void linkAuthorTest() throws DAOException {
		authorDao.linkAuthor(3l, 3l);
	}
}
