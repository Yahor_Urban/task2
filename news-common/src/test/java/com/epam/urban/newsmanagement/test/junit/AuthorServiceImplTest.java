package com.epam.urban.newsmanagement.test.junit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.urban.newsmanagement.dao.AuthorDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.imp.AuthorServiceImp;
import com.epam.urban.newsmanagement.model.Author;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {

	@InjectMocks
	private AuthorServiceImp authorServise;

	@Mock
	private AuthorDao authorDaoMock;

	@Test
	public void test() throws DAOException, LogicException {
		Author author = new Author();
		author.setId(1l);
		author.setName("name");
		author.setNewsId(1l);
		Long id[] = new Long[] {1l, 1l, 1l};
		
		when(authorDaoMock.getAuthorId(author.getName())).thenReturn(0l);
		when(authorDaoMock.saveAuthor(author)).thenReturn(true);
		when(authorDaoMock.linkAuthor(author.getNewsId(), author.getId())).thenReturn(true);
		
		assertEquals(true, authorServise.addAuthor(author));
		assertEquals(true, authorServise.save(author));
		assertEquals(true, authorServise.deleteAuthor(id));

		verify(authorDaoMock).getAuthorId(author.getName());
		verify(authorDaoMock).addAuthor(author);
		verify(authorDaoMock).saveAuthor(author);
		verify(authorDaoMock, times(3)).deleteAuthor(1l);
	}
}
