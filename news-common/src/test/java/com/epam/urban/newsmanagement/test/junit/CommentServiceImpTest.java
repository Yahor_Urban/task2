package com.epam.urban.newsmanagement.test.junit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.urban.newsmanagement.dao.CommentDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.imp.CommentServiceImp;
import com.epam.urban.newsmanagement.model.Comment;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImpTest {

	@InjectMocks
	private CommentServiceImp commentService;
	
	@Mock
	private CommentDao commentDaoMock;
	
	@Test
	public void test() throws DAOException, LogicException {
		Comment comment = new Comment();
		comment.setId(1l);
		comment.setNewsId(1l);
		comment.setCommentText("commentText");
		Long id[] = new Long[] {1l, 1l, 1l};		
		
		when(commentDaoMock.addComment(comment)).thenReturn(true);
		when(commentDaoMock.deleteCommentByNewsId(comment.getNewsId())).thenReturn(true);
		when(commentDaoMock.saveComment(comment)).thenReturn(true);
		when(commentDaoMock.deleteComment(1l)).thenReturn(true);
		
		assertEquals(true, commentService.add(comment));
		assertEquals(true, commentService.deleteCommentByNewsId(comment.getNewsId()));
		assertEquals(true, commentService.updateComment(comment));
		assertEquals(true, commentService.deleteComment(id));
		
		verify(commentDaoMock).addComment(comment);
		verify(commentDaoMock, times(3)).deleteComment(1l);
		verify(commentDaoMock).deleteCommentByNewsId(comment.getNewsId());
	}
}
