package com.epam.urban.newsmanagement.logic.service.imp;

import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.urban.newsmanagement.dao.CommentDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.CommentService;
import com.epam.urban.newsmanagement.model.Comment;

@Service
@Transactional(rollbackFor=Exception.class)
public class CommentServiceImp implements CommentService {
	
	private static final Logger LOG = Logger.getLogger(CommentServiceImp.class);
	
	@Autowired
	private CommentDao commentDao;

	/* (non-Javadoc)
	 * @see com.epam.urban.newsmanagement.logic.service.CommentService#add(com.epam.urban.newsmanagement.model.Comment)
	 */
	public boolean add(Comment comment) throws LogicException {
		if (comment.getCommentText() == null
				|| comment.getCommentText().isEmpty()) {
			LOG.error("Comment text is empty");
			throw new LogicException("Comment text is empty");
		} else if (comment.getNewsId() == null) {
			LOG.error("Incorrect newsId");
			throw new LogicException("Incorrect newsId");
		}
		boolean rezult = false;
		comment.setCreationDate(new Timestamp(System.currentTimeMillis()));

		try {
			commentDao.addComment(comment);
			rezult = true;
		} catch (DAOException e) {
			LOG.error("Adding comment failed", e);
			throw new LogicException("Adding comment failed", e);
		}

		return rezult;
	}

	/* (non-Javadoc)
	 * @see com.epam.urban.newsmanagement.logic.service.CommentService#deleteComment(java.lang.Long[])
	 */
	public boolean deleteComment(Long[] commentId) throws LogicException {
		if (commentId == null || commentId.length == 0) {
			LOG.error("Incorrect commentId");
			throw new LogicException("Incorrect commentId");
		}
		boolean rezult = false;
		try {
			for (int i = 0; i < commentId.length; i++) {
				if (!commentDao.deleteComment(commentId[i])) {
					LOG.error("Deleting comment failed");
					throw new LogicException("Deleting comment failed");
				}
			}
			rezult = true;
		} catch (DAOException e) {
			LOG.error("Deleting comment failed", e);
			throw new LogicException("Deleting comment failed", e);
		}

		return rezult;
	}
	
	/* (non-Javadoc)
	 * @see com.epam.urban.newsmanagement.logic.service.CommentService#deleteCommentByNewsId(java.lang.Long)
	 */
	public boolean deleteCommentByNewsId(Long newsId) throws LogicException{
		if (newsId == null) {
			LOG.error("News id is null");
			throw new LogicException("News id is null");
		}
		boolean result = false;
		try {
			commentDao.deleteCommentByNewsId(newsId);
			result = true;
		} catch (DAOException e) {
			LOG.error("Deleting comment failed", e);
			throw new LogicException("Deleting comment failed", e);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.epam.urban.newsmanagement.logic.service.CommentService#updateComment(com.epam.urban.newsmanagement.model.Comment)
	 */
	public boolean updateComment(Comment comment) throws LogicException {
		if (comment.getCommentText() == null
				|| comment.getCommentText().isEmpty()) {
			LOG.error("Comment text is empty");
			throw new LogicException("Comment text is empty");
		} else if (comment.getId() == null) {
			LOG.error("Incorrect commentId");
			throw new LogicException("Incorrect commentId");
		}
		boolean rezult = false;
		try {
			commentDao.saveComment(comment);
			rezult = true;
		} catch (DAOException e) {
			LOG.error("Edit comment failed");
			throw new LogicException("Edit comment failed");
		}

		return rezult;
	}

	/* (non-Javadoc)
	 * @see com.epam.urban.newsmanagement.logic.service.CommentService#getNewsCommentList(java.lang.Long)
	 */
	public List<Comment> getNewsCommentList(Long newsId) throws LogicException {
		if (newsId == null) {
			LOG.error("Incorrect newsId");
			throw new LogicException("Incorrect newsId");
		}

		try {
			return commentDao.getNewsCommentList(newsId);
		} catch (DAOException e) {
			LOG.error("Finding comment list failed");
			throw new LogicException("Finding comment list failed");
		}
	}
}
