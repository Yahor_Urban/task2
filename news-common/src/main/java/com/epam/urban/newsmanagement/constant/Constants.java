package com.epam.urban.newsmanagement.constant;

public final class Constants {

	public static final String POOLSIZE = "connection.poolsize";
	public static final String DRIVER = "connection.driver";
	public static final String URL = "connection.url";
	public static final String USER = "connection.user";
	public static final String PASSWORD = "connection.password";

	// public static final String ADD_NEWS =
	// "INSERT INTO news (short_text, full_text, title, creation_date, modification_date) VALUES (?, ?, ?, to_timestamp(?, 'YYYY-MM-DD HH:MI:SS.FF'), to_date(?, 'YYYY-MM-DD'))";
	public static final String ADD_NEWS = "INSERT INTO news (short_text, full_text, title, creation_date, modification_date) VALUES (?, ?, ?, ?, ?)";
	// public static final String EDIT_NEWS =
	// "UPDATE news SET short_text = ?, full_text = ?, title = ?, modification_date = to_date(?, 'YYYY-MM-DD') WHERE news_id = ?";
	public static final String EDIT_NEWS = "UPDATE news SET short_text = ?, full_text = ?, title = ?, modification_date = ? WHERE news_id = ?";
	public static final String DEL_NEWS = "DELETE FROM news WHERE news_id = ?";
	public static final String LIST_NEWS = "SELECT * FROM ( SELECT a.*, ROWNUM rwnum FROM (SELECT news_id, short_text, full_text, title, creation_date, name FROM news LEFT JOIN author LEFT JOIN news_author USING(author_id) USING(news_id) ORDER BY (news_id))a WHERE ROWNUM <= ? ) WHERE rwnum >= ?";
	public static final String LIST_NEWS1 = "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name FROM news LEFT JOIN author LEFT JOIN news_author USING(author_id) USING(news_id) ORDER BY (news_id)";
	public static final String LIST_NEWS2 = "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) USING(author_id) USING(news_id) ORDER BY c_count DESC NULLS LAST, creation_date DESC";
	public static final String GET_NEWS_BY_ID = "SELECT short_text, full_text, title, creation_date, modification_date, name, author_id FROM news LEFT JOIN news_author INNER JOIN author USING(author_id) USING(news_id) WHERE news_id = ?";
	public static final String GET_NEWS_ID = "SELECT news_id FROM news WHERE title = ?";
	public static final String GET_NEWS_BY_AUTHOR = "SELECT news_id, short_text, full_text, title, creation_date, modification_date FROM news_author LEFT JOIN news USING(news_id) WHERE news_author_id = ?";
	public static final String GET_NEWS_BY_TAG = "SELECT news_id, short_text, full_text, title, creation_date, modification_date FROM news_tag LEFT JOIN news USING(news_id) WHERE tag_id = ?";
	public static final String GET_NEWS_BY_TAG_AND_AUTHOR = "SELECT news_id, short_text, full_text, title, creation_date, NAME, tag_name FROM tag RIGHT JOIN news_tag RIGHT JOIN news RIGHT JOIN news_author RIGHT JOIN author USING(author_id) USING(news_id) USING(news_id) USING(tag_id) WHERE tag_id = ANY (SELECT * FROM TABLE (?)) AND author_id = ?";
	public static final String GET_COUNT_OF_NEWS = "SELECT COUNT(*) FROM news";
//	public static final String GET_NEXT_NEWS = "SELECT news_id, short_text, full_text, title, creation_date, name, ROWNUM FROM (SELECT * FROM news LEFT JOIN author LEFT JOIN news_author USING(author_id) USING(news_id) ORDER BY(news_id)) WHERE news_id > ? AND ROWNUM = 1";
//	public static final String GET_PREVIOUS_NEWS = "SELECT news_id, short_text, full_text, title, creation_date, name, ROWNUM FROM (SELECT * FROM news LEFT JOIN author LEFT JOIN news_author USING(author_id) USING(news_id) ORDER BY(news_id) DESC) WHERE news_id < ? AND ROWNUM = 1";

	
	//next or previous news
	public static final String GET_NEXT_NEWS = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name	FROM (SELECT  n.* FROM ("
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id) ) n )"
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select *"
			+ "	FROM numbered_news n1 JOIN numbered_news n2 ON n2.news_id = ? WHERE n1.num = n2.num + 1";
	public static final String GET_PREVIOUS_NEWS = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name	FROM (SELECT  n.* FROM ("
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id) ) n )"
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select *"
			+ "	FROM numbered_news n1 JOIN numbered_news n2 ON n2.news_id = ? WHERE n1.num = n2.num - 1";
	
	public static final String GET_NEXT_NEWS_BY_AUTHOR = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name	FROM (SELECT  n.* FROM ("
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id) ) n )"
			+ "WHERE author_id = ? GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select *"
			+ "	FROM numbered_news n1 JOIN numbered_news n2 ON n2.news_id = ? WHERE n1.num = n2.num + 1";
	public static final String GET_PREVIOUS_NEWS_BY_AUTHOR = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name	FROM (SELECT  n.* FROM ("
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id) ) n )"
			+ "WHERE author_id = ? GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select *"
			+ "	FROM numbered_news n1 JOIN numbered_news n2 ON n2.news_id = ? WHERE n1.num = n2.num - 1";
	
	public static final String GET_NEXT_NEWS_BY_TAG = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name	FROM (SELECT  n.* FROM ("
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id) ) n )"
			+ "WHERE tag_id = ANY (SELECT * FROM TABLE (?)) GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select *"
			+ "	FROM numbered_news n1 JOIN numbered_news n2 ON n2.news_id = ? WHERE n1.num = n2.num + 1";
	public static final String GET_PREVIOUS_NEWS_BY_TAG = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name	FROM (SELECT  n.* FROM ("
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id) ) n )"
			+ "WHERE tag_id = ANY (SELECT * FROM TABLE (?)) GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select *"
			+ "	FROM numbered_news n1 JOIN numbered_news n2 ON n2.news_id = ? WHERE n1.num = n2.num - 1";
	
	public static final String GET_NEXT_NEWS_BY_TAG_AND_AUTHOR = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name	FROM (SELECT  n.* FROM ("
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id) ) n )"
			+ "WHERE author_id = ? AND tag_id = ANY (SELECT * FROM TABLE (?)) GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select *"
			+ "	FROM numbered_news n1 JOIN numbered_news n2 ON n2.news_id = ? WHERE n1.num = n2.num + 1";
	public static final String GET_PREVIOUS_NEWS_BY_TAG_AND_AUTHOR = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name	FROM (SELECT  n.* FROM ("
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id) ) n )"
			+ "WHERE author_id = ? AND tag_id = ANY (SELECT * FROM TABLE (?)) GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select *"
			+ "	FROM numbered_news n1 JOIN numbered_news n2 ON n2.news_id = ? WHERE n1.num = n2.num - 1";
	
	//news list
	public static final String GET_LIST_NEWS = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name "
			+ "FROM (SELECT  n.* FROM ( "
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id) ) n ) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select * "
			+ "FROM numbered_news n1 WHERE n1.num BETWEEN ? AND ?";
	public static final String GET_LIST_NEWS_BY_AUTHOR = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name "
			+ "FROM (SELECT  n.* FROM ( "
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id)) n ) "
			+ "WHERE author_id = ? GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select * "
			+ "FROM numbered_news n1 WHERE n1.num BETWEEN ? AND ?";
	public static final String GET_LIST_NEWS_BY_TAG = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name "
			+ "FROM (SELECT  n.* FROM ( "
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id)) n ) "
			+ "WHERE tag_id = ANY (SELECT * FROM TABLE (?)) GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select * "
			+ "FROM numbered_news n1 WHERE n1.num BETWEEN ? AND ?";
	public static final String GET_LIST_NEWS_BY_TAG_AND_AUTHOR = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name "
			+ "FROM (SELECT  n.* FROM ( "
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id)) n ) "
			+ "WHERE author_id = ? AND tag_id = ANY (SELECT * FROM TABLE (?)) GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select * "
			+ "FROM numbered_news n1 WHERE n1.num BETWEEN ? AND ?";
	
	//count
	public static final String GET_COUNT_OF_LIST_NEWS = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name "
			+ "FROM (SELECT  n.* FROM ( "
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id) ) n ) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select count(*) "
			+ "FROM numbered_news";
	public static final String GET_COUNT_OF_LIST_NEWS_BY_AUTHOR = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name "
			+ "FROM (SELECT  n.* FROM ( "
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id)) n ) "
			+ "WHERE author_id = ? GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select count(*) "
			+ "FROM numbered_news n1";
	public static final String GET_COUNT_OF_LIST_NEWS_BY_TAG = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name "
			+ "FROM (SELECT  n.* FROM ( "
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id)) n ) "
			+ "WHERE tag_id = ANY (SELECT * FROM TABLE (?)) GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select count(*) "
			+ "FROM numbered_news n1";
	public static final String GET_COUNT_OF_LIST_NEWS_BY_TAG_AND_AUTHOR = "WITH numbered_news AS ( SELECT rownum AS num, n1.* FROM ("
			+ "select news_id, short_text, full_text, title, creation_date, modification_date, name "
			+ "FROM (SELECT  n.* FROM ( "
			+ "SELECT news_id, short_text, full_text, title, creation_date, modification_date, name, author_id, tag_id, c_count FROM news LEFT JOIN author LEFT JOIN news_author LEFT JOIN news_tag LEFT JOIN (SELECT news_id, count(*) AS c_count FROM news RIGHT JOIN comments USING(news_id) GROUP BY(news_id)) USING (news_id) Using (news_id) USING(author_id) USING(news_id) "
			+ "GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, c_count, name, author_id, tag_id)) n ) "
			+ "WHERE author_id = ? AND tag_id = ANY (SELECT * FROM TABLE (?)) GROUP BY (news_id, short_text, full_text, title, creation_date, modification_date, name, c_count) ORDER BY creation_date DESC, c_count DESC NULLS LAST) n1 ) "
			+ "select count(*) "
			+ "FROM numbered_news n1";
	
	public static final String ADD_AUTHOR = "INSERT INTO author (name) VALUES (?)";
	public static final String ADD_NEWS_AUTHOR = "INSERT INTO news_author (news_id, author_id) VALUES (?, ?)";
	public static final String GET_AUTHOR_LIST = "SELECT author_id, name FROM author";
	public static final String GET_NOT_EXPIRED_AUTHOR_LIST = "SELECT author_id, name FROM author WHERE expire = 0";
	public static final String DEL_AUTHOR = "DELETE FROM author WHERE author_id = ?";
	public static final String DEL_AUTHOR_BY_ID = "DELETE FROM news_author WHERE author_id = ?";
	public static final String DEL_NEWS_AUTHOR = "DELETE FROM news_author WHERE news_id = ?";
	public static final String EXPIRE_AUTHOR = "UPDATE author SET expire = 1 WHERE author_id = ?";
	public static final String EDIT_AUTHOR = "UPDATE author SET name = ? WHERE author_id = ?";
	public static final String GET_NEWS_AUTHOR = "SELECT name FROM author LEFT JOIN news_author USING(author_id) WHERE news_id = ?";
	public static final String UPDATE_NEWS_AUTHOR = "UPDATE news_author SET author_id = ? WHERE news_id = ?";
	
	// public static final String ADD_COMMENT =
	// "INSERT INTO comments (comment_text, creation_date, news_id) VALUES (?, to_timestamp(?, 'YYYY-MM-DD HH:MI:SS.FF'), ?)";
	public static final String ADD_COMMENT = "INSERT INTO comments (comment_text, creation_date, news_id) VALUES (?, ?, ?)";
	public static final String DEL_COMMENT = "DELETE FROM comments WHERE comment_id = ?";
	public static final String GET_COUNT = "SELECT COUNT(*) FROM comments WHERE news_id = ?";
	public static final String DEL_COMMENTS = "DELETE FROM comments WHERE news_id = ?";
	public static final String EDIT_COMMENT = "UPDATE comments SET comment_text = ? WHERE comment_id = ?";
	public static final String GET_NEWS_COMMENT_LIST = "SELECT comment_id, comment_text, creation_date FROM comments WHERE news_id = ?";

	public static final String ADD_TAG = "INSERT INTO tag (tag_name) VALUES (?)";
	public static final String GET_TAG_LIST = "SELECT tag_id, tag_name FROM tag";
	public static final String ADD_NEWS_TAG = "INSERT INTO news_tag (news_id, tag_id) VALUES (?, ?)";
	public static final String DEL_NEWS_TAG = "DELETE FROM news_tag WHERE news_id = ?";
	public static final String GET_TAG_LIST_BY_NEWS_ID = "SELECT tag_name, tag_id FROM news_tag LEFT JOIN tag USING(tag_id) WHERE news_id = ?";
	public static final String SAVE_TAG = "UPDATE tag SET tag_name = ? WHERE tag_id = ?";
	public static final String DEL_TAG_BY_ID = "DELETE FROM tag WHERE tag_id = ?";
	public static final String DEL_NEWS_TAG_BY_ID = "DELETE FROM news_tag WHERE tag_id = ?";
	
	public static final String NEWS_ID = "news_id";
	public static final String COMMENT_ID = "comment_id";
	public static final String COMMENT_TEXT = "comment_text";
	public static final String SHORT_TEXT = "short_text";
	public static final String FULL_TEXT = "full_text";
	public static final String TITLE = "title";
	public static final String CREATION_DATE = "creation_date";
	public static final String MODIFICATION_DATE = "modification_date";
	public static final String AUTHOR_ID = "author_id";
	public static final String NAME = "name";
	public static final String TAG_NAME = "tag_name";
	public static final String TAG_ID = "tag_id";

	public static final String NEXT = "next";
	public static final String PREVIOUS = "previous";

	public static final int MAX_NEWS = 2;

	public static final String DATE_REGEX_EN = "^(?:(((Jan?|Mar?|May?|Jul?|Aug?|Oct?|Dec?)\\ 31)|((Jan?|Mar?|Apr?|May?|Jun?|Jul?|Aug?|Oct?|Sep?|Nov?|Dec?)\\ (0?[1-9]|([12]\\d)|30))|(Feb?\\ (0?[1-9]|1\\d|2[0-8]|(29(?=,\\ ((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)))))))\\,\\ ((1[6-9]|[2-9]\\d)\\d{2}))";
	public static final String DATE_REGEX_RU = "^(?:(?:31(\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";
	public static final String DATE_PATTERN_EN = "MMM dd, yyyy";
	public static final String DATE_PATTERN_RU = "dd.mm.yyyy";
	
	public static final String RESOURCES_PATH = "database";

	private Constants() {
	}
}
