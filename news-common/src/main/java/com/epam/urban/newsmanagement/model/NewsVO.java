package com.epam.urban.newsmanagement.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class NewsVO implements Serializable {

	private static final long serialVersionUID = -7681665613748751718L;
	
	private News news;
	private Author author;
	private List<Tag> tags;
	private int countOfComments;
	private List<Comment> comments;

	public NewsVO() {
		tags = new LinkedList<Tag>();
	}
	
	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void addTags(Tag tag) {
		this.tags.add(tag);
	}
	
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public int getCountOfComments() {
		return countOfComments;
	}

	public void setCountOfComments(int countOfComments) {
		this.countOfComments = countOfComments;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + countOfComments;
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsVO other = (NewsVO) obj;
//		if (author == null) {
//			if (other.author != null)
//				return false;
//		} else if (!author.equals(other.author))
//			return false;
//		if (comments == null) {
//			if (other.comments != null)
//				return false;
//		} else if (!comments.equals(other.comments))
//			return false;
//		if (countOfComments != other.countOfComments)
//			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
//		if (tags == null) {
//			if (other.tags != null)
//				return false;
//		} else if (!tags.equals(other.tags))
//			return false;
		return true;
	}
	
	
}
