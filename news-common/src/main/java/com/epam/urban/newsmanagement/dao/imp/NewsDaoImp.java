package com.epam.urban.newsmanagement.dao.imp;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.dao.NewsDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.Author;
import com.epam.urban.newsmanagement.model.Filter;
import com.epam.urban.newsmanagement.model.News;
import com.epam.urban.newsmanagement.model.NewsVO;
import com.epam.urban.newsmanagement.model.Tag;

/**
 * News dao class
 * 
 * @author Urban Egor
 * 
 */
@Repository
public class NewsDaoImp extends BaseDao implements NewsDao {

	private static final Logger LOG = Logger.getLogger(NewsDaoImp.class);

	/**
	 * Add news
	 * 
	 * @param news
	 * @return {@code true}, if news successfully added
	 * @throws DAOException
	 */
	public boolean addNews(News news) throws DAOException {
		if (news == null) {
			LOG.error("News is null!");
			throw new DAOException("News is null!");
		}
		boolean result = false;
		Connection connection = getConnection();
		PreparedStatement addNewsStatement = null;
		ResultSet resultSet = null;
		try {
			addNewsStatement = openPrepStatement(connection, Constants.ADD_NEWS, new String[] { Constants.NEWS_ID });
			addNewsStatement.setString(1, news.getShortText());
			addNewsStatement.setString(2, news.getFullText());
			addNewsStatement.setString(3, news.getTitle());
			addNewsStatement.setTimestamp(4, (Timestamp) news.getCreationDate());
			addNewsStatement.setDate(5, (Date) news.getModificationDate());
			addNewsStatement.execute();
			resultSet = addNewsStatement.getGeneratedKeys();
			if (resultSet.next()) {
				news.setId(resultSet.getLong(1));
			}
			result = true;
		} catch (SQLException e) {
			LOG.error("Adding news failed", e);
			throw new DAOException("Adding news failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, addNewsStatement);
		}

		return result;
	}

	/**
	 * Edit news
	 * 
	 * @param news
	 * @return {@code true}, if news successfully changed
	 * @throws DAOException
	 */
	public boolean saveNews(News news) throws DAOException {
		if (news == null) {
			LOG.error("News is null!");
			throw new DAOException("News is null!");
		}
		boolean result = false;
		Connection connection = getConnection();
		PreparedStatement editNewsStatement = null;

		try {
			editNewsStatement = openPrepStatement(connection, Constants.EDIT_NEWS);
			editNewsStatement.setString(1, news.getShortText());
			editNewsStatement.setString(2, news.getFullText());
			editNewsStatement.setString(3, news.getTitle());
			editNewsStatement.setDate(4, (Date) news.getModificationDate());
			editNewsStatement.setLong(5, news.getId());
			editNewsStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Edit news failed", e);
			throw new DAOException("Edit news failed", e);
		} finally {
			close(connection, editNewsStatement);
		}

		return result;
	}

	/**
	 * Delete news by id
	 * 
	 * @param id
	 * @return {@code true}, if news successfully deleted
	 * @throws DAOException
	 */
	public boolean deleteNews(Long id) throws DAOException {
		boolean result = false;
		Connection connection = getConnection();
		PreparedStatement delNewsStatement = null;

		try {
			delNewsStatement = openPrepStatement(connection, Constants.DEL_NEWS);
			delNewsStatement.setLong(1, id);
			delNewsStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Deleting news failed", e);
			throw new DAOException("Deleting news failed", e);
		} finally {
			close(connection, delNewsStatement);
		}

		return result;
	}

	/**
	 * To get news list
	 * 
	 * @return {@code List<News>} news list
	 * @throws DAOException
	 */
	public List<NewsVO> newsList(int newsMin, int newsMax, Filter filter) throws DAOException {
		List<NewsVO> list = new LinkedList<NewsVO>();
		String query = null;
		Long id = 0l;
		Connection connection = getConnection();
		PreparedStatement getNewsListStatement = null;
		PreparedStatement getTagListStatement = null;
		ResultSet resultSet = null;
		ResultSet resultSet1 = null;
		if (filter == null
				|| (filter.getCheckedAuthor() == null && (filter.getCheckedTags() == null || filter.getCheckedTags().length == 0))) {
			query = Constants.GET_LIST_NEWS;
		} else if (filter.getCheckedAuthor() != null
				&& (filter.getCheckedTags() == null || filter.getCheckedTags().length == 0)) {
			query = Constants.GET_LIST_NEWS_BY_AUTHOR;
		} else if (filter.getCheckedAuthor() == null
				&& (filter.getCheckedTags() != null && filter.getCheckedTags().length != 0)) {
			query = Constants.GET_LIST_NEWS_BY_TAG;
		} else {
			query = Constants.GET_LIST_NEWS_BY_TAG_AND_AUTHOR;
		}
		try {
			getNewsListStatement = openPrepStatement(connection, query);

			switch (query) {
			case Constants.GET_LIST_NEWS_BY_AUTHOR:
				getNewsListStatement.setLong(1, filter.getCheckedAuthor());
				getNewsListStatement.setInt(2, newsMin);
				getNewsListStatement.setInt(3, newsMax);
				break;
			case Constants.GET_LIST_NEWS_BY_TAG:
				getNewsListStatement.setArray(1, getTagArray(filter.getCheckedTags()));
				getNewsListStatement.setInt(2, newsMin);
				getNewsListStatement.setInt(3, newsMax);
				break;
			case Constants.GET_LIST_NEWS_BY_TAG_AND_AUTHOR:
				getNewsListStatement.setLong(1, filter.getCheckedAuthor());
				getNewsListStatement.setArray(2, getTagArray(filter.getCheckedTags()));
				getNewsListStatement.setInt(3, newsMin);
				getNewsListStatement.setInt(4, newsMax);
				break;
			default:
				getNewsListStatement.setInt(1, newsMin);
				getNewsListStatement.setInt(2, newsMax);
				break;
			}
			resultSet = getNewsListStatement.executeQuery();
			while (resultSet.next()) {
				NewsVO newsVO = new NewsVO();
				Long newsId = resultSet.getLong(Constants.NEWS_ID);
				if (!id.equals(newsId)) {
					id = newsId;
					News news = new News();
					Author author = new Author();
					news.setId(newsId);
					news.setShortText(resultSet.getString(Constants.SHORT_TEXT));
					news.setFullText(resultSet.getString(Constants.FULL_TEXT));
					news.setTitle(resultSet.getString(Constants.TITLE));
					news.setCreationDate(resultSet.getTimestamp(Constants.CREATION_DATE));
					author.setName(resultSet.getString(Constants.NAME));
					newsVO.setCountOfComments(getCountOfComments(newsId));
					newsVO.setNews(news);
					newsVO.setAuthor(author);
					getTagListStatement = openPrepStatement(connection, Constants.GET_TAG_LIST_BY_NEWS_ID);
					getTagListStatement.setLong(1, newsId);
					resultSet1 = getTagListStatement.executeQuery();
					while (resultSet1.next()) {
						Tag tag = new Tag();
						tag.setTagName(resultSet1.getString(Constants.TAG_NAME));
						newsVO.addTags(tag);
					}
					list.add(newsVO);
				}
			}

		} catch (SQLException e) {
			LOG.error("Finding news list failed", e);
			throw new DAOException("Finding news list failed", e);
		} finally {
			closeResultSet(resultSet, resultSet1);
			close(connection, getNewsListStatement, getTagListStatement);
		}

		return list;
	}

	public List<NewsVO> getNewsList() throws DAOException {
		List<NewsVO> list = new ArrayList<NewsVO>();
		Connection connection = getConnection();
		Statement getNewsListStatement = null;
		PreparedStatement getTagListStatement = null;
		ResultSet resultSet = null;
		ResultSet resultSet1 = null;
		try {
			// getNewsListStatement = openPrepStatement(connection, Constants.LIST_NEWS1);
			// resultSet = getNewsListStatement.executeQuery();
			getNewsListStatement = openStatement(connection);
			resultSet = getNewsListStatement.executeQuery(Constants.LIST_NEWS2);
			while (resultSet.next()) {
				NewsVO newsVO = new NewsVO();
				Long newsId = resultSet.getLong(Constants.NEWS_ID);
				News news = new News();
				Author author = new Author();
				news.setId(newsId);
				news.setShortText(resultSet.getString(Constants.SHORT_TEXT));
				news.setFullText(resultSet.getString(Constants.FULL_TEXT));
				news.setTitle(resultSet.getString(Constants.TITLE));
				news.setCreationDate(resultSet.getTimestamp(Constants.CREATION_DATE));
				news.setModificationDate(resultSet.getDate(Constants.MODIFICATION_DATE));
				author.setName(resultSet.getString(Constants.NAME));
				newsVO.setCountOfComments(getCountOfComments(newsId));
				newsVO.setNews(news);
				newsVO.setAuthor(author);
				getTagListStatement = openPrepStatement(connection, Constants.GET_TAG_LIST_BY_NEWS_ID);
				getTagListStatement.setLong(1, newsId);
				resultSet1 = getTagListStatement.executeQuery();
				while (resultSet1.next()) {
					Tag tag = new Tag();
					tag.setTagName(resultSet1.getString(Constants.TAG_NAME));
					newsVO.addTags(tag);
				}
				list.add(newsVO);
			}

		} catch (SQLException e) {
			LOG.error("Finding news list failed", e);
			throw new DAOException("Finding news list failed", e);
		} finally {
			closeResultSet(resultSet, resultSet1);
			close(connection, getNewsListStatement, getTagListStatement);
		}

		return list;
	}

	public NewsVO getNextOrPreviousNews(Long newsId, String type, Filter filter) throws DAOException {
		String query = null;
		Connection connection = getConnection();
		switch (type) {
		case Constants.NEXT:
			if (filter == null
					|| (filter.getCheckedAuthor() == null && (filter.getCheckedTags() == null || filter
							.getCheckedTags().length == 0))) {
				query = Constants.GET_NEXT_NEWS;
			} else if (filter.getCheckedAuthor() != null
					&& (filter.getCheckedTags() == null || filter.getCheckedTags().length == 0)) {
				query = Constants.GET_NEXT_NEWS_BY_AUTHOR;
			} else if (filter.getCheckedAuthor() == null
					&& (filter.getCheckedTags() != null && filter.getCheckedTags().length != 0)) {
				query = Constants.GET_NEXT_NEWS_BY_TAG;
			} else {
				query = Constants.GET_NEXT_NEWS_BY_TAG_AND_AUTHOR;
			}
			break;
		case Constants.PREVIOUS:
			if (filter == null
					|| (filter.getCheckedAuthor() == null && (filter.getCheckedTags() == null || filter
							.getCheckedTags().length == 0))) {
				query = Constants.GET_PREVIOUS_NEWS;
			} else if (filter.getCheckedAuthor() != null
					&& (filter.getCheckedTags() == null || filter.getCheckedTags().length == 0)) {
				query = Constants.GET_PREVIOUS_NEWS_BY_AUTHOR;
			} else if (filter.getCheckedAuthor() == null
					&& (filter.getCheckedTags() != null && filter.getCheckedTags().length != 0)) {
				query = Constants.GET_PREVIOUS_NEWS_BY_TAG;
			} else {
				query = Constants.GET_PREVIOUS_NEWS_BY_TAG_AND_AUTHOR;
			}
			break;
		default:
			LOG.error("Wrong type");
			throw new DAOException("Wrong type");
		}
		NewsVO newsVO = null;
		PreparedStatement nextNewsStatement = null;
		PreparedStatement newsTagsStatement = null;
		ResultSet resultSet = null;
		ResultSet resultSet1 = null;
		try {
			nextNewsStatement = openPrepStatement(connection, query);
			switch (query) {
			case Constants.GET_NEXT_NEWS:
			case Constants.GET_PREVIOUS_NEWS:
				nextNewsStatement.setLong(1, newsId);
				break;
			case Constants.GET_NEXT_NEWS_BY_AUTHOR:
			case Constants.GET_PREVIOUS_NEWS_BY_AUTHOR:
				nextNewsStatement.setLong(1, filter.getCheckedAuthor());
				nextNewsStatement.setLong(2, newsId);
				break;
			case Constants.GET_NEXT_NEWS_BY_TAG:
			case Constants.GET_PREVIOUS_NEWS_BY_TAG:
				nextNewsStatement.setArray(1, getTagArray(filter.getCheckedTags()));
				nextNewsStatement.setLong(2, newsId);
				break;
			case Constants.GET_NEXT_NEWS_BY_TAG_AND_AUTHOR:
			case Constants.GET_PREVIOUS_NEWS_BY_TAG_AND_AUTHOR:
				nextNewsStatement.setLong(1, filter.getCheckedAuthor());
				nextNewsStatement.setArray(2, getTagArray(filter.getCheckedTags()));
				nextNewsStatement.setLong(3, newsId);
				break;
			default:
				break;
			}

			resultSet = nextNewsStatement.executeQuery();
			while (resultSet.next()) {
				newsVO = new NewsVO();
				News news = new News();
				Author author = new Author();
				news.setId(resultSet.getLong(Constants.NEWS_ID));
				news.setShortText(resultSet.getString(Constants.SHORT_TEXT));
				news.setFullText(resultSet.getString(Constants.FULL_TEXT));
				news.setTitle(resultSet.getString(Constants.TITLE));
				news.setCreationDate(resultSet.getTimestamp(Constants.CREATION_DATE));
				author.setName(resultSet.getString(Constants.NAME));
				newsVO.setNews(news);
				newsVO.setAuthor(author);
				newsVO.setCountOfComments(getCountOfComments(newsId));
				newsTagsStatement = openPrepStatement(connection, Constants.GET_TAG_LIST_BY_NEWS_ID);
				newsTagsStatement.setLong(1, resultSet.getLong(Constants.NEWS_ID));
				resultSet1 = newsTagsStatement.executeQuery();
				while (resultSet1.next()) {
					Tag tag = new Tag();
					tag.setTagName(resultSet1.getString(Constants.TAG_NAME));
					newsVO.addTags(tag);
				}
			}

		} catch (SQLException e) {
			LOG.error("Finding news list failed", e);
			throw new DAOException("Finding news list failed", e);
		} finally {
			closeResultSet(resultSet, resultSet1);
			close(connection, nextNewsStatement, newsTagsStatement);
		}

		return newsVO;
	}

	/**
	 * To get news by id
	 * 
	 * @param id
	 * @return {@code News} single news
	 * @throws DAOException
	 */
	public NewsVO getNewsById(Long id) throws DAOException {
		Connection connection = getConnection();
		PreparedStatement getNewsByIdStatement = null;
		ResultSet resultSet = null;
		NewsVO newsVO = null;
		try {
			getNewsByIdStatement = openPrepStatement(connection, Constants.GET_NEWS_BY_ID);
			getNewsByIdStatement.setLong(1, id);
			resultSet = getNewsByIdStatement.executeQuery();
			while (resultSet.next()) {
				newsVO = new NewsVO();
				News news = new News();
				Author author = new Author();
				news.setId(id);
				news.setShortText(resultSet.getString(Constants.SHORT_TEXT));
				news.setFullText(resultSet.getString(Constants.FULL_TEXT));
				news.setTitle(resultSet.getString(Constants.TITLE));
				author.setName(resultSet.getString(Constants.NAME));
				author.setId(resultSet.getLong(Constants.AUTHOR_ID));
				news.setCreationDate(resultSet.getTimestamp(Constants.CREATION_DATE));
				news.setModificationDate(resultSet.getDate(Constants.MODIFICATION_DATE));
				newsVO.setNews(news);
				newsVO.setAuthor(author);
			}

		} catch (SQLException e) {
			LOG.error("Finding news by id failed", e);
			throw new DAOException("Finding news by id failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getNewsByIdStatement);
		}
		return newsVO;
	}

	/**
	 * To get news list by author id
	 * 
	 * @param author
	 *            id
	 * @return {@code List<News>} list news by author
	 * @throws DAOException
	 */
	public List<News> getNewsByAuthor(Long authorId) throws DAOException {
		List<News> list = new LinkedList<News>();
		Connection connection = getConnection();
		PreparedStatement getNewsByAuthorStatement = null;
		ResultSet resultSet = null;
		try {
			getNewsByAuthorStatement = openPrepStatement(connection, Constants.GET_NEWS_BY_AUTHOR);
			getNewsByAuthorStatement.setLong(1, authorId);
			resultSet = getNewsByAuthorStatement.executeQuery();
			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(Constants.NEWS_ID));
				news.setShortText(resultSet.getString(Constants.SHORT_TEXT));
				news.setFullText(resultSet.getString(Constants.FULL_TEXT));
				news.setTitle(resultSet.getString(Constants.TITLE));
				news.setCreationDate(resultSet.getTimestamp(Constants.CREATION_DATE));
				news.setModificationDate(resultSet.getDate(Constants.MODIFICATION_DATE));
				list.add(news);
			}

		} catch (SQLException e) {
			LOG.error("Finding news list by author failed", e);
			throw new DAOException("Finding news list by author failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getNewsByAuthorStatement);
		}

		return list;
	}

	/**
	 * To get news list by tag id
	 * 
	 * @param tag
	 *            id
	 * @return {@code List<News>} list news by tag
	 * @throws DAOException
	 */
	public List<News> getNewsByTag(Long tagId) throws DAOException {
		List<News> list = new LinkedList<News>();
		Connection connection = getConnection();
		PreparedStatement getNewsByTagStatement = null;
		ResultSet resultSet = null;
		try {
			getNewsByTagStatement = openPrepStatement(connection, Constants.GET_NEWS_BY_TAG);
			getNewsByTagStatement.setLong(1, tagId);
			resultSet = getNewsByTagStatement.executeQuery();
			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong(Constants.NEWS_ID));
				news.setShortText(resultSet.getString(Constants.SHORT_TEXT));
				news.setFullText(resultSet.getString(Constants.FULL_TEXT));
				news.setTitle(resultSet.getString(Constants.TITLE));
				news.setCreationDate(resultSet.getTimestamp(Constants.CREATION_DATE));
				news.setModificationDate(resultSet.getDate(Constants.MODIFICATION_DATE));
				list.add(news);
			}

		} catch (SQLException e) {
			LOG.error("Finding news list by tag failed", e);
			throw new DAOException("Finding news list by tag failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getNewsByTagStatement);
		}

		return list;
	}

	private ARRAY getTagArray(Long[] checkedTags) throws DAOException {
		Connection connection = getConnection();
		ArrayDescriptor descriptor = null;
		ARRAY array = null;
		try {
			descriptor = ArrayDescriptor.createDescriptor("LONGINTLIST", connection);
			array = new ARRAY(descriptor, connection, checkedTags);
		} catch (SQLException e1) {
			LOG.error("Creating array failed", e1);
			throw new DAOException("Creating array failed", e1);
		} finally {
			closeConnection(connection);
		}
		return array;
	}

	public List<NewsVO> getNewsByTagAndAuthor(Long authorId, Long[] tagId) throws DAOException {
		List<NewsVO> list = new ArrayList<NewsVO>();
		Connection connection = getConnection();
		PreparedStatement getNewsStatement = null;
		ResultSet resultSet = null;
		Long id = 0l;
		ArrayDescriptor descriptor = null;
		ARRAY array = null;
		try {
			descriptor = ArrayDescriptor.createDescriptor("LONGINTLIST", connection);
			array = new ARRAY(descriptor, connection, tagId);
		} catch (SQLException e1) {
			LOG.error("Creating array failed", e1);
			throw new DAOException("Creating array failed", e1);
		}

		try {
			getNewsStatement = openPrepStatement(connection, Constants.GET_NEWS_BY_TAG_AND_AUTHOR);
			getNewsStatement.setArray(1, array);
			getNewsStatement.setLong(2, authorId);
			resultSet = getNewsStatement.executeQuery();
			while (resultSet.next()) {
				Long newsId = resultSet.getLong(Constants.NEWS_ID);
				if (!id.equals(newsId)) {
					NewsVO newsVO = new NewsVO();
					News news = new News();
					Author author = new Author();
					Tag tag = new Tag();
					news.setId(newsId);
					news.setShortText(resultSet.getString(Constants.SHORT_TEXT));
					news.setFullText(resultSet.getString(Constants.FULL_TEXT));
					news.setTitle(resultSet.getString(Constants.TITLE));
					news.setCreationDate(resultSet.getTimestamp(Constants.CREATION_DATE));
					author.setName(resultSet.getString(Constants.NAME));
					tag.setTagName(resultSet.getString(Constants.TAG_NAME));
					newsVO.addTags(tag);
					newsVO.setNews(news);
					newsVO.setAuthor(author);
					list.add(newsVO);
					id = newsId;
				} else {
					Tag tag = new Tag();
					tag.setTagName(resultSet.getString(Constants.TAG_NAME));
					list.get(list.size() - 1).addTags(tag);
				}
			}

		} catch (SQLException e) {
			LOG.error("Finding news list by tag and author failed", e);
			throw new DAOException("Finding news list by tag and author failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getNewsStatement);
		}
		return list;
	}

	/**
	 * To get count of one news comments
	 * 
	 * @param news
	 *            id
	 * @return {@code int} Count
	 * @throws DAOException
	 */
	public int getCountOfComments(Long newsId) throws DAOException {
		Connection connection = getConnection();
		PreparedStatement getCountStatement = null;
		ResultSet resultSet = null;
		try {
			getCountStatement = openPrepStatement(connection, Constants.GET_COUNT);
			getCountStatement.setLong(1, newsId);
			resultSet = getCountStatement.executeQuery();
			if (resultSet.next()) {
				return resultSet.getInt(1);
			}
		} catch (SQLException e) {
			LOG.error("Getting count failed", e);
			throw new DAOException("Getting count failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getCountStatement);
		}

		return -1;

	}

	public int getCountOfNews(Filter filter) throws DAOException {
		Connection connection = getConnection();
		String query = null;
		PreparedStatement getCountOfNewsStatement = null;
		ResultSet resultSet = null;
		if (filter == null
				|| (filter.getCheckedAuthor() == null && (filter.getCheckedTags() == null || filter.getCheckedTags().length == 0))) {
			query = Constants.GET_COUNT_OF_LIST_NEWS;
		} else if (filter.getCheckedAuthor() != null
				&& (filter.getCheckedTags() == null || filter.getCheckedTags().length == 0)) {
			query = Constants.GET_COUNT_OF_LIST_NEWS_BY_AUTHOR;
		} else if (filter.getCheckedAuthor() == null
				&& (filter.getCheckedTags() != null && filter.getCheckedTags().length != 0)) {
			query = Constants.GET_COUNT_OF_LIST_NEWS_BY_TAG;
		} else {
			query = Constants.GET_COUNT_OF_LIST_NEWS_BY_TAG_AND_AUTHOR;
		}
		try {
			getCountOfNewsStatement = openPrepStatement(connection, query);
			switch (query) {
			case Constants.GET_COUNT_OF_LIST_NEWS_BY_AUTHOR:
				getCountOfNewsStatement.setLong(1, filter.getCheckedAuthor());
				break;
			case Constants.GET_COUNT_OF_LIST_NEWS_BY_TAG:
				getCountOfNewsStatement.setArray(1, getTagArray(filter.getCheckedTags()));
				break;
			case Constants.GET_COUNT_OF_LIST_NEWS_BY_TAG_AND_AUTHOR:
				getCountOfNewsStatement.setLong(1, filter.getCheckedAuthor());
				getCountOfNewsStatement.setArray(2, getTagArray(filter.getCheckedTags()));
				break;
			default:
				break;
			}
			resultSet = getCountOfNewsStatement.executeQuery();
			if (resultSet.next()) {
				return resultSet.getInt(1);
			}
		} catch (SQLException e) {
			LOG.error("Getting count of news failed", e);
			throw new DAOException("Getting count of news failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getCountOfNewsStatement);
		}

		return -1;
	}

	public Long getNewsId(String title) throws DAOException {
		Connection connection = getConnection();
		PreparedStatement getNewsIdStatement = null;
		ResultSet resultSet = null;
		try {
			getNewsIdStatement = openPrepStatement(connection, Constants.GET_NEWS_ID);
			getNewsIdStatement.setString(1, title);
			resultSet = getNewsIdStatement.executeQuery();
			if (resultSet.next()) {
				return resultSet.getLong(Constants.NEWS_ID);
			}
		} catch (SQLException e) {
			LOG.error("Getting count failed", e);
			throw new DAOException("Getting count failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getNewsIdStatement);
		}

		return 0l;
	}

}
