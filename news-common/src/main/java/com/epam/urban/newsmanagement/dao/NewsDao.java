package com.epam.urban.newsmanagement.dao;

import java.util.List;

import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.Filter;
import com.epam.urban.newsmanagement.model.News;
import com.epam.urban.newsmanagement.model.NewsVO;

public interface NewsDao {

	public boolean addNews(News news) throws DAOException;

	public boolean saveNews(News news) throws DAOException;

	public boolean deleteNews(Long id) throws DAOException;

	public List<NewsVO> newsList(int newsMin, int newsMax, Filter filter) throws DAOException;
	
	public List<NewsVO> getNewsList() throws DAOException;

	public NewsVO getNextOrPreviousNews(Long newsId, String type, Filter filter) throws DAOException;

	public NewsVO getNewsById(Long id) throws DAOException;

	public List<News> getNewsByAuthor(Long authorId) throws DAOException;

	public List<News> getNewsByTag(Long tagId) throws DAOException;

	public List<NewsVO> getNewsByTagAndAuthor(Long authorId, Long[] tagId) throws DAOException;

	public int getCountOfComments(Long newsId) throws DAOException;

	public int getCountOfNews(Filter filter) throws DAOException;

	public Long getNewsId(String title) throws DAOException;

}
