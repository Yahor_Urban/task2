package com.epam.urban.newsmanagement.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.dao.CommentDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.Comment;

/**
 * Comment dao class
 * 
 * @author Urban Egor
 * 
 */
@Repository
public class CommentDaoImp extends BaseDao implements CommentDao {

	private static final Logger LOG = Logger.getLogger(CommentDaoImp.class);
	
	/**
	 * Add comment
	 * 
	 * @param comment
	 * @return {@code true}, if comment successfully added
	 * @throws DAOException
	 */
	public boolean addComment(Comment comment) throws DAOException {
		boolean result = false;
		Connection connection = getConnection();
		PreparedStatement addCommentStatement = null;

		try {
			addCommentStatement = openPrepStatement(connection, Constants.ADD_COMMENT);
			addCommentStatement.setString(1, comment.getCommentText());
			addCommentStatement.setTimestamp(2, (Timestamp) comment.getCreationDate());
			addCommentStatement.setLong(3, comment.getNewsId());
			addCommentStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Adding comment failed", e);
			throw new DAOException("Adding comment failed", e);
		} finally {
			close(connection, addCommentStatement);
		}

		return result;
	}

	/**
	 * Delete comment by id
	 * 
	 * @param comment
	 *            id
	 * @return {@code true}, if comment successfully deleted
	 * @throws DAOException
	 */
	public boolean deleteComment(Long commentId) throws DAOException {
		boolean result = false;
		Connection connection = getConnection();
		PreparedStatement delCommentStatement = null;

		try {
			delCommentStatement = openPrepStatement(connection, Constants.DEL_COMMENT);
			delCommentStatement.setLong(1, commentId);
			delCommentStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Delleting comment failed", e);
			throw new DAOException("Delleting comment failed", e);
		} finally {
			close(connection, delCommentStatement);
		}

		return result;
	}

	public boolean deleteCommentByNewsId(Long newsId) throws DAOException {
		if (newsId == null) {
			throw new DAOException("News id is null");
		}
		boolean result = false;

		Connection connection = getConnection();
		PreparedStatement delCommentsStatement = null;
		try {
			delCommentsStatement = openPrepStatement(connection, Constants.DEL_COMMENTS);
			delCommentsStatement.setLong(1, newsId);
			delCommentsStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Delleting comments failed", e);
			throw new DAOException("Delleting comments failed", e);
		} finally {
			close(connection, delCommentsStatement);
		}

		return result;
	}

	public boolean saveComment(Comment comment) throws DAOException {
		boolean result = false;
		Connection connection = getConnection();
		PreparedStatement saveCommentStatement = null;

		try {
			saveCommentStatement = openPrepStatement(connection, Constants.EDIT_COMMENT);
			saveCommentStatement.setLong(2, comment.getId());
			saveCommentStatement.setString(1, comment.getCommentText());
			saveCommentStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Edit comment failed", e);
			throw new DAOException("Edit comment failed", e);
		} finally {
			close(connection, saveCommentStatement);
		}

		return result;
	}

	public List<Comment> getNewsCommentList(Long newsId) throws DAOException {
		Connection connection = getConnection();
		PreparedStatement getNewsCommentListStatement = null;
		LinkedList<Comment> commentList = new LinkedList<Comment>();
		Comment comment = null;
		ResultSet resultSet = null;
		try {
			getNewsCommentListStatement = openPrepStatement(connection, Constants.GET_NEWS_COMMENT_LIST);
			getNewsCommentListStatement.setLong(1, newsId);
			resultSet = getNewsCommentListStatement.executeQuery();
			while (resultSet.next()) {
				comment = new Comment();
				comment.setNewsId(newsId);
				comment.setId(resultSet.getLong(Constants.COMMENT_ID));
				comment.setCommentText(resultSet.getString(Constants.COMMENT_TEXT));
				comment.setCreationDate(resultSet.getTimestamp(Constants.CREATION_DATE));
				commentList.add(comment);
			}
		} catch (SQLException e) {
			LOG.error("Finding comment list failed", e);
			throw new DAOException("Finding comment list failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getNewsCommentListStatement);
		}
		return commentList;
	}

}
