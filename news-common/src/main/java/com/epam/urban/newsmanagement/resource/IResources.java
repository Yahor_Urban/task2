package com.epam.urban.newsmanagement.resource;

public interface IResources {

	public String getString(String key);
	
	public boolean containsKey(String key);
	
}
