package com.epam.urban.newsmanagement.dao.imp;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.urban.newsmanagement.exception.DAOException;

/**
 * Abstract dao class
 * 
 * @author Urban Egor
 * 
 */

public abstract class BaseDao {

	private static final Logger LOG = Logger.getLogger(BaseDao.class);

	protected DataSource dataSource;
//	protected IResources resources = DatabaseResources.getInstance();

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	protected Statement openStatement(Connection connection) throws SQLException {

		return connection.createStatement();
	}
	
	protected PreparedStatement openPrepStatement(Connection connection, String sqlPattern) throws SQLException {
		PreparedStatement statement = null;
		// String sql = resources.getString(sqlPattern);
		statement = connection.prepareStatement(sqlPattern);
		return statement;
	}

	protected PreparedStatement openPrepStatement(Connection connection, String sqlPattern, String[] columnIndexes)
			throws SQLException {
		PreparedStatement statement = null;
		// String sql = resources.getString(sqlPattern);
		statement = connection.prepareStatement(sqlPattern, columnIndexes);
		return statement;
	}

	//unused
	protected CallableStatement openCallStatement(Connection connection, String sqlPattern) throws SQLException {
		CallableStatement statement = null;
		// String sql = resources.getString(sqlPattern);
		statement = connection.prepareCall(sqlPattern);
		return statement;
	}

	protected void closeStatement(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				LOG.error("Statement closing failed", e);
			}
		}
	}
	
	protected void closeConnection(Connection conn) {
		DataSourceUtils.releaseConnection(conn, dataSource);
	}

	protected Connection getConnection() throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		return connection;
	}
	
	protected void closeResultSet(ResultSet... resultSet) throws DAOException {
		List<String> errors = new ArrayList<String>();
		if (resultSet != null) {
			for (int i = 0; i < resultSet.length; i++) {
				if (resultSet[i] != null) {
					try {
						resultSet[i].close();
					} catch (SQLException e) {
						LOG.error("ResultSet closing failed", e);
						errors.add(e.toString());
					}
				}
			}
			if (!errors.isEmpty()) {
				throw new DAOException("Errors on closing ResultSet: " + errors.toString());
			}
		}
	}
	
	protected void close(Connection conn, Statement... statement) throws DAOException {
		List<String> errors = new ArrayList<String>();
		if (statement != null) {
			for (int i = 0; i < statement.length; i++) {
				if (statement[i] != null) {
					try {
						statement[i].close();
					} catch (SQLException e) {
						LOG.error("Statement closing failed", e);
						errors.add(e.toString());
					}
				}
			}
			if (!errors.isEmpty()) {
				throw new DAOException("Errors on closing database resources: " + errors.toString());
			}
		}
		DataSourceUtils.releaseConnection(conn, dataSource);
	}
}
