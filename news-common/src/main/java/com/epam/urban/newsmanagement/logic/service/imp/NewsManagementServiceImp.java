package com.epam.urban.newsmanagement.logic.service.imp;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.AuthorService;
import com.epam.urban.newsmanagement.logic.service.CommentService;
import com.epam.urban.newsmanagement.logic.service.NewsManagementService;
import com.epam.urban.newsmanagement.logic.service.NewsService;
import com.epam.urban.newsmanagement.logic.service.TagService;
import com.epam.urban.newsmanagement.model.Filter;
import com.epam.urban.newsmanagement.model.News;
import com.epam.urban.newsmanagement.model.NewsVO;

@Service
@Transactional(rollbackFor=Exception.class)
public class NewsManagementServiceImp implements NewsManagementService {

	private static final Logger LOG = Logger.getLogger(NewsManagementServiceImp.class);
	
	@Autowired
	private NewsService newsService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;
	@Autowired
	private CommentService commentService;
	
	public void delete(Long[] newsId) throws LogicException {
		for (Long id : newsId) {
			try {
				newsService.deleteNews(id);
				authorService.deleteAuthorByNewsId(id);
				tagService.deleteTagByNewId(id);
				commentService.deleteCommentByNewsId(id);
			} catch (LogicException e) {
				LOG.error("Deleting failed", e);
				throw new LogicException("Deleting failed", e);
			}
		}
	}
	
	public List<NewsVO> getSortedNewsList(/*int newsMin, int newsMax*/) throws LogicException {
		List<NewsVO> news = null;
//		news = newsService.getNewsList(newsMin, newsMax);
		news = newsService.getNewsList();
		for (NewsVO n : news) {
			n.setComments(commentService.getNewsCommentList(n.getNews().getId()));
		}
//		Collections.sort(news, new Comparator<NewsVO>() {
//
//			@Override
//			public int compare(NewsVO o1, NewsVO o2) {
//				return o1.getCountOfComments() > o2.getCountOfComments() ? -1 : o1.getCountOfComments() == o2
//						.getCountOfComments() ? 0 : 1;
//			}
//
//		});

		return news;
	}
	
	public void add(NewsVO newsVO) throws LogicException {
		if (newsVO == null || newsVO.getAuthor().getId() == null || newsVO.getTags() == null) {
			LOG.error("Incorrect data!");
			throw new LogicException("Incorrect data!");
		}
		newsService.add(newsVO.getNews());
		newsService.link(newsVO);
	}
	
	public void updateNews(News news, Filter filter) throws LogicException {
		if (news == null || filter == null) {
			LOG.error("Incorrect data!");
			throw new LogicException("Incorrect data!");
		}
		
		newsService.save(news);
		authorService.updateNewsAuthor(news.getId(), filter.getCheckedAuthor());
		tagService.deleteTagByNewId(news.getId());
		for (Long tagId : filter.getCheckedTags()) {
			tagService.linkTag(news.getId(), tagId);
		}
	}
	
	public NewsVO getSingleNews(Long newsId) throws LogicException {
		if (newsId == null) {
			LOG.error("News id is null!");
			throw new LogicException("News id is null!");
		}
		NewsVO newsVO = null;
		newsVO = newsService.getSingleNewsMessage(newsId);
		newsVO.setTags(tagService.getTagListByNewsId(newsId));
		
		return newsVO;
	}
	
	
}
