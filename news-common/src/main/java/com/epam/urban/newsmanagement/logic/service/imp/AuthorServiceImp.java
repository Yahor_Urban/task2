package com.epam.urban.newsmanagement.logic.service.imp;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.urban.newsmanagement.dao.AuthorDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.AuthorService;
import com.epam.urban.newsmanagement.model.Author;

/**
 * Author service class
 * 
 * @author Urban Egor
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AuthorServiceImp implements AuthorService {

	private static final Logger LOG = Logger.getLogger(AuthorServiceImp.class);

	@Autowired
	private AuthorDao authorDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.AuthorService#linkAuthor(com.epam
	 * .urban.newsmanagement.model.Author)
	 */
	public boolean linkAuthor(Author author) throws LogicException {
		if (author.getName() == null || author.getName().isEmpty()) {
			LOG.error("Author name is empty");
			throw new LogicException("Author name is empty");
		} else if (author.getNewsId() == null) {
			LOG.error("Incorrect newsId");
			throw new LogicException("Incorrect newsId");
		}
		boolean result = false;
		try {
			if (addAuthor(author)) {
				if (!authorDao.linkAuthor(author.getNewsId(), author.getId())) {
					LOG.error("Adding news author failed");
					throw new LogicException("Adding news author failed");
				}
				result = true;
			}
		} catch (DAOException e) {
			LOG.error("Adding news author failed", e);
			throw new LogicException("Adding news author failed", e);
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.AuthorService#addAuthor(com.epam
	 * .urban.newsmanagement.model.Author)
	 */
	public boolean addAuthor(Author author) throws LogicException {
		if (author.getName() == null || author.getName().isEmpty()) {
			LOG.error("Author name is empty");
			throw new LogicException("Author name is empty");
		}
		boolean result = false;
		try {
			if (authorDao.getAuthorId(author.getName()) == 0) {
				authorDao.addAuthor(author);
				result = true;
			} else {
				LOG.error("News author is exist");
				throw new LogicException("News author is exist");
			}
		} catch (DAOException e) {
			LOG.error("Adding news author failed", e);
			throw new LogicException("Adding news author failed", e);
		}
		return result;
	}

	public boolean updateNewsAuthor(Long newsId, Long authorId) throws LogicException {
		if (newsId == null || authorId == null) {
			LOG.error("Incorrect data");
			throw new LogicException("Incorrect data");
		}
		
		try {
			authorDao.updateNewsAuthor(newsId, authorId);
			return true;
		} catch (DAOException e) {
			LOG.error("Updating news author failed", e);
			throw new LogicException("Updating news author failed", e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.AuthorService#save(com.epam .urban.newsmanagement.model.Author)
	 */
	public boolean save(Author author) throws LogicException {
		if (author.getId() == null || author.getName() == null || author.getName().isEmpty()) {
			LOG.error("Incorrect data");
			throw new LogicException("Incorrect data");
		}
		boolean result = false;

		try {
			authorDao.saveAuthor(author);
			result = true;
		} catch (DAOException e) {
			LOG.error("Editing news author failed", e);
			throw new LogicException("Editing news author failed", e);
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.AuthorService#deleteAuthor(java .lang.Long[])
	 */
	public boolean deleteAuthor(Long[] id) throws LogicException {
		if (id == null || id.length == 0) {
			LOG.error("Author id is null");
			throw new LogicException("Author id is null");
		}

		try {
			for (int i = 0; i < id.length; i++) {
				authorDao.deleteAuthor(id[i]);
			}
		} catch (DAOException e) {
			LOG.error("Deleting news author failed", e);
			throw new LogicException("Deleting news author failed", e);
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.AuthorService#deleteAuthorByNewsId(java.lang.Long)
	 */
	public boolean deleteAuthorByNewsId(Long newsId) throws LogicException {
		if (newsId == null) {
			LOG.error("News id is null");
			throw new LogicException("News id is null");
		}

		try {
			return authorDao.deleteAuthor(newsId);
		} catch (DAOException e) {
			LOG.error("Deleting news author failed", e);
			throw new LogicException("Deleting news author failed", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.AuthorService#getNewsAuthor (java.lang.Long)
	 */
	public String getNewsAuthor(Long id) throws LogicException {
		if (id == null) {
			LOG.error("Author id is null");
			throw new LogicException("Author id is null");
		}
		try {
			return authorDao.getNewsAuthorById(id);
		} catch (DAOException e) {
			LOG.error("Getting autor name failed");
			throw new LogicException("Getting autor name failed");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.AuthorService#getList()
	 */
	public List<Author> getList() throws LogicException {
		List<Author> list = null;
		try {
			list = authorDao.getAuthorList();
		} catch (DAOException e) {
			LOG.error("Finding author list failed");
			throw new LogicException("Finding author list failed");
		}
		return list;
	}
	
	public List<Author> getNotExpiredAuthorList() throws LogicException {
		List<Author> list = null;
		try {
			list = authorDao.getNotExpiredAuthorList();
		} catch (DAOException e) {
			LOG.error("Finding author list failed");
			throw new LogicException("Finding author list failed");
		}
		return list;
	}
	
	public boolean expireAuthor(Long id) throws LogicException {
		if (id == null) {
			LOG.error("Author id is null");
			throw new LogicException("Author id is null");
		}
		
		try {
			authorDao.expireAuthor(id);
		} catch (DAOException e) {
			LOG.error("Expiring author failed");
			throw new LogicException("Expiring author failed");
		}
		return true;
	}
}
