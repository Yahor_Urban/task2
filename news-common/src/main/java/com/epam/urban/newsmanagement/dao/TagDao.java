package com.epam.urban.newsmanagement.dao;

import java.util.List;

import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.Tag;

public interface TagDao {

	public void addTag(Tag tag) throws DAOException;

	public boolean saveTag(Tag tag) throws DAOException;

	public boolean linkTag(Long newsId, Long tagId) throws DAOException;

	public Long getTagId(String tagName) throws DAOException;

	public List<Tag> getTagList() throws DAOException;

	public List<Tag> getTagListByNewsId(Long newsId) throws DAOException;

	public boolean deleteTagByNewsId(Long newsId) throws DAOException;

	public boolean deleteTagById(Long tagId) throws DAOException;

	public boolean unlinkTagAndNews(Long tagId) throws DAOException;

}