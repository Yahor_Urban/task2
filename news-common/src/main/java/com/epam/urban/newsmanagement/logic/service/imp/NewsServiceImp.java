package com.epam.urban.newsmanagement.logic.service.imp;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.dao.AuthorDao;
import com.epam.urban.newsmanagement.dao.NewsDao;
import com.epam.urban.newsmanagement.dao.TagDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.NewsService;
import com.epam.urban.newsmanagement.model.Filter;
import com.epam.urban.newsmanagement.model.News;
import com.epam.urban.newsmanagement.model.NewsVO;
import com.epam.urban.newsmanagement.model.Tag;

/**
 * News service class
 * 
 * @author Urban Egor
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class NewsServiceImp implements NewsService {

	private static final Logger LOG = Logger.getLogger(NewsServiceImp.class);
	
	@Autowired
	private NewsDao newsDao;
	@Autowired
	private AuthorDao authorDao;
	@Autowired
	private TagDao tagDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.NewsService#add(com.epam. urban.newsmanagement.model.News)
	 */
	public boolean add(News news) throws LogicException {
		if (news == null) {
			LOG.error("News is null");
			throw new LogicException("News is null");
		}

		try {
			if (!newsDao.addNews(news)) {
				return false;
			}
		} catch (DAOException e) {
			LOG.error("Adding news failed", e);
			throw new LogicException("Adding news failed", e);
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.NewsService#save(com.epam .urban.newsmanagement.model.News)
	 */
	public boolean save(News news) throws LogicException {
		if (news == null) {
			LOG.error("News is null");
			throw new LogicException("News is null");
		}
//		java.util.Date date = new java.util.Date();
//		long t = date.getTime();
//		java.sql.Date sqlDate = new java.sql.Date(t);
//		news.setModificationDate(sqlDate);
		try {
			if (!newsDao.saveNews(news)) {
				return false;
			}
		} catch (DAOException e) {
			LOG.error("Edit news failed", e);
			throw new LogicException("Edit news failed", e);
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.NewsService#deleteNews(java.lang .Long)
	 */
	public boolean deleteNews(Long newsId) throws LogicException {
		boolean flag = false;
		if (newsId == null) {
			LOG.error("Incorrect newsId");
			throw new LogicException("Incorrect newsId");
		}

		try {
			if (!newsDao.deleteNews(newsId)) {
				LOG.error("Deleting news failed");
				throw new LogicException("Deleting news failed");
			}
			flag = true;
		} catch (DAOException e) {
			LOG.error("Deleting news failed", e);
			throw new LogicException("Deleting news failed", e);
		}
		return flag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.NewsService#getNewsList(int)
	 */
	public List<NewsVO> getNewsList(int newsMin, int newsMax, Filter filter) throws LogicException {
		if (newsMin <= 0 || newsMax <= 0) {
			LOG.error("Incorrect data");
			throw new LogicException("Incorrect data");
		}
		List<NewsVO> list = null;
		try {
			list = newsDao.newsList(newsMin, newsMax, filter);
			if (list == null) {
				LOG.error("News list is null");
				throw new LogicException("News list is null");
			}
		} catch (DAOException e) {
			LOG.error("Getting news list failed", e);
			throw new LogicException("Getting news list failed", e);
		}

		return list;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.NewsService#getNewsList()
	 */
	public List<NewsVO> getNewsList() throws LogicException {
		List<NewsVO> list = null;
		try {
			list = newsDao.getNewsList();
			if (list == null) {
				LOG.error("News list is null");
				throw new LogicException("News list is null");
			}
		} catch (DAOException e) {
			LOG.error("Getting news list failed", e);
			throw new LogicException("Getting news list failed", e);
		}
		
		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.NewsService#getSingleNewsMessage (java.lang.Long)
	 */
	public NewsVO getSingleNewsMessage(Long newsId) throws LogicException {
		if (newsId < 0 || newsId == null) {
			LOG.error("Incorrect newsId");
			throw new LogicException("Incorrect newsId");
		}
		NewsVO newsVO = null;
		try {
			newsVO = newsDao.getNewsById(newsId);
			if (newsVO == null) {
				LOG.error("Single news message is null");
				throw new LogicException("Single news message is null");
			}
		} catch (DAOException e) {
			LOG.error("Getting news list failed", e);
			throw new LogicException("Getting news list failed", e);
		}

		return newsVO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.NewsService#getNewsByAuthor (java.lang.String)
	 */
	// unused
	public List<News> getNewsByAuthor(String authorName) throws LogicException {
		if (authorName == null || authorName.isEmpty()) {
			LOG.error("Author name is empty");
			throw new LogicException("Author name is empty");
		}
		List<News> list = new ArrayList<News>();

		try {
			Long authorId = authorDao.getAuthorId(authorName);
			if (authorId == 0) {
				LOG.error("Author is not exist");
				throw new LogicException("Author is not exist");
			} else {
				list = newsDao.getNewsByAuthor(authorId);
			}
		} catch (DAOException e) {
			LOG.error("Gettin news by author failed", e);
			throw new LogicException("Gettin news by author failed", e);
		}

		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.NewsService#getNewsByTag( java.lang.String)
	 */
	// unused
	public List<News> getNewsByTag(String tagName) throws LogicException {
		if (tagName == null || tagName.isEmpty()) {
			LOG.error("Tag name is empty");
			throw new LogicException("Tag name is empty");
		}
		List<News> list = new ArrayList<News>();

		try {
			Long tagId = tagDao.getTagId(tagName);
			if (tagId == -1) {
				LOG.error("Tag is not exist");
				throw new LogicException("Tag is not exist");
			} else {
				list = newsDao.getNewsByTag(tagId);
			}
		} catch (DAOException e) {
			LOG.error("Gettin news by tag failed", e);
			throw new LogicException("Gettin news by tag failed", e);
		}

		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.NewsService#link(com.epam. urban.newsmanagement.model.NewsVO)
	 */
	public void link(NewsVO newsVO) throws LogicException {
		if (newsVO == null || newsVO.getAuthor() == null || newsVO.getTags() == null) {
			LOG.error("Incorrect data!");
			throw new LogicException("Incorrect data!");
		}
		try {
			authorDao.linkAuthor(newsVO.getNews().getId(), newsVO.getAuthor().getId());
			for (Tag tag : newsVO.getTags()) {
				tagDao.linkTag(newsVO.getNews().getId(), tag.getId());
			}
		} catch (DAOException e) {
			LOG.error("Linking failed", e);
			throw new LogicException("Linking failed", e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.NewsService# getListNewsByTagAndAuthor(java.lang.Long,
	 * java.lang.Long[])
	 */
	public List<NewsVO> getListNewsByTagAndAuthor(Long authorId, Long[] tagId) throws LogicException {
//		assertObjectNotNull(authorId, "Author ID can not be null");
		if (authorId == null || tagId == null) {
			LOG.error("Incorrect data!");
			throw new LogicException("Incorrect data!");
		}
		List<NewsVO> list = null;
		try {
			list = newsDao.getNewsByTagAndAuthor(authorId, tagId);
		} catch (DAOException e) {
			LOG.error("Getting news list by tag and author failed", e);
			throw new LogicException("Getting news list by tag and author failed", e);
		}

		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.NewsService#getCountOfNews()
	 */
	public int getCountOfNews(Filter filter) throws LogicException {
		int count = 0;
		try {
			count = newsDao.getCountOfNews(filter);
		} catch (DAOException e) {
			LOG.error("Getting count of news failed", e);
			throw new LogicException("Getting count of news failed", e);
		}

		return count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.NewsService#getNextNews(java.lang.Long)
	 */
	public NewsVO getNextNews(Long newsId, Filter filter) throws LogicException {
		if (newsId == null) {
			LOG.error("News id is null");
			throw new LogicException("News id is null");
		}
		NewsVO newsVO = null;

		try {
			newsVO = newsDao.getNextOrPreviousNews(newsId, Constants.NEXT, filter);
			if (newsVO == null) {
				newsVO = getSingleNewsMessage(newsId);
			}
		} catch (DAOException e) {
			LOG.error("Finding next news failed", e);
			throw new LogicException("Finding next news failed", e);
		}

		return newsVO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.NewsService#getPreviousNews(java.lang.Long)
	 */
	public NewsVO getPreviousNews(Long newsId, Filter filter) throws LogicException {
		if (newsId == null) {
			LOG.error("News id is null");
			throw new LogicException("News id is null");
		}
		NewsVO newsVO = null;

		try {
			newsVO = newsDao.getNextOrPreviousNews(newsId, Constants.PREVIOUS, filter);
			if (newsVO == null) {
				newsVO = getSingleNewsMessage(newsId);
			}
		} catch (DAOException e) {
			LOG.error("Finding previous news failed", e);
			throw new LogicException("Finding previous news failed", e);
		}

		return newsVO;
	}

	
}
