package com.epam.urban.newsmanagement.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.dao.AuthorDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.Author;

/**
 * Author dao class
 * 
 * @author Urban Egor
 * 
 */
@Repository
public class AuthorDaoImp extends BaseDao implements AuthorDao {

	private static final Logger LOG = Logger.getLogger(AuthorDaoImp.class);
	
	/**
	 * Add single author
	 * 
	 * @param author
	 *            name
	 * @return author id
	 * @throws DAOException
	 */
	public void addAuthor(Author author) throws DAOException {
		Connection connection = getConnection();
		PreparedStatement addAuthorStatement = null;
		ResultSet resultSet = null;
		try {
			addAuthorStatement = openPrepStatement(connection, Constants.ADD_AUTHOR, new String[] {Constants.AUTHOR_ID});
			addAuthorStatement.setString(1, author.getName());
			addAuthorStatement.execute();
			resultSet = addAuthorStatement.getGeneratedKeys();
			if (resultSet.next()) {
				author.setId(resultSet.getLong(1));
			}
		} catch (SQLException e) {
			LOG.error("Adding author failed", e);
			throw new DAOException("Adding author failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, addAuthorStatement);
		}

	}

	/**
	 * Add author by news id
	 * 
	 * @param news
	 *            id
	 * @param author
	 *            id
	 * @return {@code true}, if author successfully added
	 * @throws DAOException
	 */
	public boolean linkAuthor(Long newsId, Long authorId) throws DAOException {
		boolean result = false;
		Connection connection = getConnection();
		PreparedStatement addNewsStatement = null;

		try {
			addNewsStatement = openPrepStatement(connection, Constants.ADD_NEWS_AUTHOR);
			addNewsStatement.setLong(1, newsId);
			addNewsStatement.setLong(2, authorId);
			addNewsStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Adding author failed", e);
			throw new DAOException("Adding author failed", e);
		} finally {
			close(connection, addNewsStatement);
		}

		return result;
	}
	
	public boolean updateNewsAuthor(Long newsId, Long authorId) throws DAOException {
		boolean result = false;
		Connection connection = getConnection();
		PreparedStatement updateStatement = null;
		
		try {
			updateStatement = openPrepStatement(connection, Constants.UPDATE_NEWS_AUTHOR);
			updateStatement.setLong(1, authorId);
			updateStatement.setLong(2, newsId);
			updateStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Updating author failed", e);
			throw new DAOException("Updating author failed", e);
		} finally {
			close(connection, updateStatement);
		}
		return result;
	}

	/**
	 * Edit author by id
	 * 
	 * @param author
	 * @return {@code true}, if successfully changed
	 * @throws DAOException
	 */
	public boolean saveAuthor(Author author) throws DAOException {
		if (author == null) {
			throw new DAOException("Author is null");
		}
		boolean result = false;

		Connection connection = getConnection();
		PreparedStatement saveAuthorStatement = null;

		try {
			saveAuthorStatement = openPrepStatement(connection, Constants.EDIT_AUTHOR);
			saveAuthorStatement.setLong(2, author.getId());
			saveAuthorStatement.setString(1, author.getName());
			saveAuthorStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Save author failed", e);
			throw new DAOException("Save author failed", e);
		} finally {
			close(connection, saveAuthorStatement);
		}

		return result;
	}

	/**
	 * Delete author by author id
	 * 
	 * @param id
	 * @return {@code true}, if successfully deleted
	 * @throws DAOException
	 */
	public boolean deleteAuthor(Long id) throws DAOException {
		if (id == null) {
			throw new DAOException("Author id is null");
		}
		boolean result = false;

		Connection connection = getConnection();
		PreparedStatement delAuthorStatement = null;
		PreparedStatement delAuthorByIdStatement = null;

		try {
			delAuthorStatement = openPrepStatement(connection, Constants.DEL_AUTHOR);
			delAuthorByIdStatement = openPrepStatement(connection, Constants.DEL_AUTHOR_BY_ID);
			delAuthorStatement.setLong(1, id);
			delAuthorByIdStatement.setLong(1, id);
			delAuthorStatement.executeUpdate();
			delAuthorByIdStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Delleting author failed", e);
			throw new DAOException("Delleting author failed", e);
		} finally {
			close(connection, delAuthorStatement, delAuthorByIdStatement);
		}

		return result;
	}
	
	public boolean expireAuthor(Long id) throws DAOException {
		if (id == null) {
			throw new DAOException("Author id is null");
		}
		boolean result = false;

		Connection connection = getConnection();
		PreparedStatement expireAuthorStatement = null;
		
		try {
			expireAuthorStatement = openPrepStatement(connection, Constants.EXPIRE_AUTHOR);
			expireAuthorStatement.setLong(1, id);
			expireAuthorStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Expiring author failed", e);
			throw new DAOException("Expiring author failed", e);
		} finally {
			close(connection, expireAuthorStatement);
		}

		return result;
	}

	@Override
	public boolean deleteAuthorByNewsId(Long newsId) throws DAOException {
		if (newsId == null) {
			throw new DAOException("News id is null");
		}
		boolean result = false;

		Connection connection = getConnection();
		PreparedStatement delNewsAuthorStatement = null;
		try {
			delNewsAuthorStatement = openPrepStatement(connection, Constants.DEL_NEWS_AUTHOR);
			delNewsAuthorStatement.setLong(1, newsId);
			delNewsAuthorStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Delleting author failed", e);
			throw new DAOException("Delleting author failed", e);
		} finally {
			close(connection, delNewsAuthorStatement);
		}

		return result;
	}

	/**
	 * To getting author id by name
	 * 
	 * @param author
	 *            name
	 * @return author id
	 * @throws DAOException
	 */
	public Long getAuthorId(String authorName) throws DAOException {
		Connection connection = getConnection();
		Statement getAuthorListStatement = null;
		ResultSet resultSet = null;
		Long authorId = 0l;
		try {
//			getAuthorListStatement = openPrepStatement(connection, Constants.GET_AUTHOR_LIST);
//			resultSet = getAuthorListStatement.executeQuery();
			getAuthorListStatement = openStatement(connection);
			resultSet = getAuthorListStatement.executeQuery(Constants.GET_AUTHOR_LIST);
			while (resultSet.next()) {
				String name = resultSet.getString(Constants.NAME);
				if (authorName.equalsIgnoreCase(name)) {
					authorId = resultSet.getLong(Constants.AUTHOR_ID);
					break;
				}
			}
		} catch (SQLException e) {
			LOG.error("Getting autor id failed", e);
			throw new DAOException("Getting autor id failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getAuthorListStatement);
		}

		return authorId;
	}

	public String getNewsAuthorById(Long newsId) throws DAOException {
		Connection connection = getConnection();
		Statement getNewsAuthorStatement = null;
		ResultSet resultSet = null;
		try {
//			getNewsAuthorStatement = openPrepStatement(connection, Constants.GET_NEWS_AUTHOR);
//			resultSet = getNewsAuthorStatement.executeQuery();
			getNewsAuthorStatement = openStatement(connection);
			resultSet = getNewsAuthorStatement.executeQuery(Constants.GET_NEWS_AUTHOR);
			while (resultSet.next()) {
				String name = resultSet.getString(Constants.NAME);
				return name;
			}
		} catch (SQLException e) {
			LOG.error("Getting autor name failed", e);
			throw new DAOException("Getting autor name failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getNewsAuthorStatement);
		}

		return null;
	}

	public List<Author> getAuthorList() throws DAOException {
		LinkedList<Author> authorList = new LinkedList<Author>();
		Connection connection = getConnection();
		Statement getAuthorListStatement = null;
		ResultSet resultSet = null;

		try {
//			getAuthorListStatement = openPrepStatement(connection, Constants.GET_AUTHOR_LIST);
//			resultSet = getAuthorListStatement.executeQuery();
			getAuthorListStatement = openStatement(connection);
			resultSet = getAuthorListStatement.executeQuery(Constants.GET_AUTHOR_LIST);
			while (resultSet.next()) {
				Author author = new Author();
				author.setId(resultSet.getLong(Constants.AUTHOR_ID));
				author.setName(resultSet.getString(Constants.NAME));
				authorList.add(author);
			}

		} catch (SQLException e) {
			LOG.error("Finding author list failed", e);
			throw new DAOException("Finding author list failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getAuthorListStatement);
		}

		return authorList;
	}
	
	public List<Author> getNotExpiredAuthorList() throws DAOException {
		List<Author> authorList = new LinkedList<Author>();
		Connection connection = getConnection();
		Statement getAuthorListStatement = null;
		ResultSet resultSet = null;
		
		try {
			getAuthorListStatement = openStatement(connection);
			resultSet = getAuthorListStatement.executeQuery(Constants.GET_NOT_EXPIRED_AUTHOR_LIST);
			while (resultSet.next()) {
				Author author = new Author();
				author.setId(resultSet.getLong(Constants.AUTHOR_ID));
				author.setName(resultSet.getString(Constants.NAME));
				authorList.add(author);
			}

		} catch (SQLException e) {
			LOG.error("Finding author list failed", e);
			throw new DAOException("Finding author list failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getAuthorListStatement);
		}
		return authorList;
	}

}
