package com.epam.urban.newsmanagement.dao;

import java.util.List;

import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.Comment;

public interface CommentDao {

	public boolean addComment(Comment comment) throws DAOException;

	public boolean deleteComment(Long commentId) throws DAOException;

	public boolean saveComment(Comment comment) throws DAOException;

	public List<Comment> getNewsCommentList(Long newsId) throws DAOException;

	boolean deleteCommentByNewsId(Long newsId) throws DAOException;

}
