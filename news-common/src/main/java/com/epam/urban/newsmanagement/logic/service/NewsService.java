package com.epam.urban.newsmanagement.logic.service;

import java.util.List;

import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.model.Filter;
import com.epam.urban.newsmanagement.model.News;
import com.epam.urban.newsmanagement.model.NewsVO;

public interface NewsService {

	/**
	 * Add news
	 * 
	 * @param {@code News} news
	 * @return {@code true}, if successfully added
	 * @throws LogicException
	 */
	public boolean add(News news) throws LogicException;

	/**
	 * Save news
	 * 
	 * @param {@code News} news
	 * @return {@code true}, if successfully saved
	 * @throws LogicException
	 */
	public boolean save(News news) throws LogicException;

	/**
	 * Delete news by news id
	 * 
	 * @param {@code Long} newsId
	 * @return {@code true}, if successfully deleted
	 * @throws LogicException
	 */
	public boolean deleteNews(Long newsId) throws LogicException;

	/**
	 * Get news list by min and max news numbers
	 * 
	 * @param {@code int} min news number
	 * @param {@code int} max news number
	 * @return {@code List<NewsVO>} news list
	 * @throws LogicException
	 */
	public List<NewsVO> getNewsList(int newsMin, int newsMax, Filter filter) throws LogicException;

	/**
	 * Get news list
	 * 
	 * @return {@code List<NewsVO>} news list
	 * @throws LogicException
	 */
	public List<NewsVO> getNewsList() throws LogicException;
	
	/**
	 * To get single news by news id
	 * 
	 * @param {@code Long} news id
	 * @return {@code NewsVO} single news
	 * @throws LogicException
	 */
	public NewsVO getSingleNewsMessage(Long newsId) throws LogicException;

	/**
	 * To get news list by author id (unused)
	 * 
	 * @param {@code String} author name
	 * @return {@code List<News>} list news by author
	 * @throws LogicException
	 */
	public List<News> getNewsByAuthor(String authorName) throws LogicException;

	/**
	 * To get news list by tag id (unused)
	 * 
	 * @param {@code String} tag name
	 * @return {@code List<News>} list news by tag
	 * @throws LogicException
	 */
	public List<News> getNewsByTag(String tagName) throws LogicException;

	/**
	 * To get sorted by count of comments news list (unused)
	 * 
	 * @return {@code List<News>} list of sorted news
	 * @throws LogicException
	 */
//	public List<News> getSortedNewsList(int newsMin, int newsMax) throws LogicException;

	/**
	 * Link news with comment and tags
	 * 
	 * @param {@code NewsVO} news with comment and tags
	 * @throws LogicException
	 */
	public void link(NewsVO newsVO) throws LogicException;

	/**
	 * Get news list by tags and author
	 * 
	 * @param {@code Long} author id
	 * @param {@code Long[]} tag id
	 * @return {@code List<NewsVO>} news list
	 * @throws LogicException
	 */
	public List<NewsVO> getListNewsByTagAndAuthor(Long authorId, Long[] tagId) throws LogicException;

	/**
	 * Get count of news
	 * 
	 * @return {@code int}
	 * @throws LogicException
	 */
	public int getCountOfNews(Filter filter) throws LogicException;

	/**
	 * Get next news by id
	 * 
	 * @param {@code Long} news id
	 * @return {@code NewsVO} single news
	 * @throws LogicException
	 */
	public NewsVO getNextNews(Long newsId, Filter filter) throws LogicException;

	/**
	 * Get previous news by id
	 * 
	 * @param {@code Long} news id
	 * @return {@code NewsVO} single news
	 * @throws LogicException
	 */
	public NewsVO getPreviousNews(Long newsId, Filter filter) throws LogicException;

}