package com.epam.urban.newsmanagement.logic.service;

import java.util.List;

import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.model.Comment;

public interface CommentService {

	/**
	 * Add comment
	 * 
	 * @param {@code Comment} comment
	 * @return {@code true}, if successfully added
	 * @throws LogicException
	 */
	public boolean add(Comment comment) throws LogicException;

	/**
	 * Delete comment
	 * 
	 * @param {@code Long[]} commentId
	 * @return {@code true}, if successfully deleted
	 * @throws LogicException
	 */
	public boolean deleteComment(Long[] commentId) throws LogicException;

	/**
	 * Update comment
	 * 
	 * @param {@code Comment} comment
	 * @return {@code true}, if successfully updated
	 * @throws LogicException
	 */
	public boolean updateComment(Comment comment) throws LogicException;

	/**
	 * Get news comment list
	 * 
	 * @param {@code Long} newsId
	 * @return {@code List<Comment>} news comment list
	 * @throws LogicException
	 */
	public List<Comment> getNewsCommentList(Long newsId)
			throws LogicException;

	/**
	 * Delete comment by news id
	 * 
	 * @param {@code Long} newsId
	 * @return {@code true}, if successfully deleted
	 * @throws LogicException
	 */
	public boolean deleteCommentByNewsId(Long newsId) throws LogicException;

}