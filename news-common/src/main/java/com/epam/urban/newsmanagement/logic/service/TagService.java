package com.epam.urban.newsmanagement.logic.service;

import java.util.List;

import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.model.Tag;

public interface TagService {

	/**
	 * Add tag
	 * 
	 * @param {@code Tag} tag
	 * @return {@code true}, if successfully added
	 * @throws LogicException
	 */
	public boolean addTag(Tag tag) throws LogicException;

	/**
	 * Link tag
	 * 
	 * @param {@code Tag} tag
	 * @return {@code true}, if successfully linked
	 * @throws LogicException
	 */
	public boolean linkTag(Tag tag) throws LogicException;
	
	public boolean linkTag(Long newsId, Long tagId) throws LogicException;

	/**
	 * Save tag
	 * 
	 * @param {@code Tag} tag
	 * @return {@code true}, if successfully saved
	 * @throws LogicException
	 */
	public boolean save(Tag tag) throws LogicException;

	/**
	 * Delete tag
	 * 
	 * @param {@code Long} tag id
	 * @return {@code true}, if successfully deleted
	 * @throws LogicException
	 */
	public boolean deleteTag(Long id) throws LogicException;

	/**
	 * Get tag list
	 * 
	 * @return {@code List<Tag>} tag list
	 * @throws LogicException
	 */
	public List<Tag> getTagList() throws LogicException;

	/**
	 * Get tag list by news id
	 * 
	 * @param {@code} news id
	 * @return {@code List<Tag>} tag list
	 * @throws LogicException
	 */
	public List<Tag> getTagListByNewsId(Long id) throws LogicException;

	/**
	 * Delete tag by news id
	 * 
	 * @param {@code Long} news id
	 * @return {@code true}, if successfully deleted
	 * @throws LogicException
	 */
	public boolean deleteTagByNewId(Long newsId) throws LogicException;
	

}