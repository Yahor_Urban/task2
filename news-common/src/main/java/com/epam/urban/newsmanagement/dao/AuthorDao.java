package com.epam.urban.newsmanagement.dao;

import java.util.List;

import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.Author;

public interface AuthorDao {

	public void addAuthor(Author author) throws DAOException;

	public boolean linkAuthor(Long newsId, Long authorId) throws DAOException;

	public boolean saveAuthor(Author author) throws DAOException;
	
	public boolean updateNewsAuthor(Long newsId, Long authorId) throws DAOException;

	public boolean deleteAuthor(Long id) throws DAOException;

	public Long getAuthorId(String authorName) throws DAOException;

	public String getNewsAuthorById(Long newsId) throws DAOException;

	public List<Author> getAuthorList() throws DAOException;

	public boolean deleteAuthorByNewsId(Long newsId) throws DAOException;
	
	public List<Author> getNotExpiredAuthorList() throws DAOException;
	
	public boolean expireAuthor(Long id) throws DAOException;

}
