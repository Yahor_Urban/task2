package com.epam.urban.newsmanagement.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.epam.urban.newsmanagement.constant.Constants;
import com.epam.urban.newsmanagement.dao.TagDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.model.Tag;

/**
 * Tag dao class
 * 
 * @author Urban Egor
 * 
 */
@Repository
public class TagDaoImp extends BaseDao implements TagDao {

	private static final Logger LOG = Logger.getLogger(TagDaoImp.class);
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.dao.imp.TagDao#addTag(java.lang.String)
	 */
	public void addTag(Tag tag) throws DAOException {
		Connection connection = getConnection();
		PreparedStatement addTagStatement = null;
		ResultSet resultSet = null;
		try {
			addTagStatement = openPrepStatement(connection, Constants.ADD_TAG, new String[] { Constants.TAG_ID });
			addTagStatement.setString(1, tag.getTagName());
			addTagStatement.execute();
			resultSet = addTagStatement.getGeneratedKeys();
			if (resultSet.next()) {
				tag.setId(resultSet.getLong(1));
			}
		} catch (SQLException e) {
			LOG.error("Adding tag failed", e);
			throw new DAOException("Adding tag failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, addTagStatement);
		}

	}

	public boolean saveTag(Tag tag) throws DAOException {
		Connection connection = getConnection();
		boolean rezult = false;
		PreparedStatement saveTagStatement = null;
		try {
			saveTagStatement = openPrepStatement(connection, Constants.SAVE_TAG);
			saveTagStatement.setString(1, tag.getTagName());
			saveTagStatement.setLong(2, tag.getId());
			saveTagStatement.executeUpdate();
			rezult = true;
		} catch (SQLException e) {
			LOG.error("Save tag failed", e);
			throw new DAOException("Save tag failed", e);
		} finally {
			close(connection, saveTagStatement);
		}
		return rezult;
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.dao.imp.TagDao#addTag(java.lang.Long, java.lang.Long)
	 */
	public boolean linkTag(Long newsId, Long tagId) throws DAOException {
		boolean result = false;
		Connection connection = getConnection();
		PreparedStatement addNewsTagStatement = null;

		try {
			addNewsTagStatement = openPrepStatement(connection, Constants.ADD_NEWS_TAG);
			addNewsTagStatement.setLong(1, newsId);
			addNewsTagStatement.setLong(2, tagId);
			addNewsTagStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Adding tag failed", e);
			throw new DAOException("Adding tag failed", e);
		} finally {
			close(connection, addNewsTagStatement);
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.dao.imp.TagDao#getTagId(java.lang.String)
	 */
	public Long getTagId(String tagName) throws DAOException {
		Connection connection = getConnection();
		Statement getTagListStatement = null;
		ResultSet resultSet = null;
		Long tagId = 0l;
		try {
//			getTagListStatement = openPrepStatement(connection, Constants.GET_TAG_LIST);
//			resultSet = getTagListStatement.executeQuery();
			getTagListStatement = openStatement(connection);
			resultSet = getTagListStatement.executeQuery(Constants.GET_TAG_LIST);
			while (resultSet.next()) {
				String name = resultSet.getString(Constants.TAG_NAME);
				if (tagName.equalsIgnoreCase(name)) {
					tagId = resultSet.getLong(Constants.TAG_ID);
					break;
				}
			}
		} catch (SQLException e) {
			LOG.error("Getting tag id failed", e);
			throw new DAOException("Getting tag id failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getTagListStatement);
		}

		return tagId;
	}

	public boolean deleteTagByNewsId(Long newsId) throws DAOException {
		if (newsId == null) {
			LOG.error("News id is null");
			throw new DAOException("News id is null");
		}
		boolean result = false;

		Connection connection = getConnection();
		PreparedStatement delNewsTagStatement = null;
		try {
			delNewsTagStatement = openPrepStatement(connection, Constants.DEL_NEWS_TAG);
			delNewsTagStatement.setLong(1, newsId);
			delNewsTagStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Delleting tags failed", e);
			throw new DAOException("Delleting tags failed", e);
		} finally {
			close(connection, delNewsTagStatement);
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.dao.imp.TagDao#getTagList()
	 */
	public List<Tag> getTagList() throws DAOException {
		LinkedList<Tag> tagList = new LinkedList<Tag>();
		Connection connection = getConnection();
		Statement getTagListStatement = null;
		ResultSet resultSet = null;
		try {
//			getTagListStatement = openPrepStatement(connection, Constants.GET_TAG_LIST);
//			resultSet = getTagListStatement.executeQuery();
			getTagListStatement = openStatement(connection);
			resultSet = getTagListStatement.executeQuery(Constants.GET_TAG_LIST);
			while (resultSet.next()) {
				Tag tag = new Tag();
				tag.setId(resultSet.getLong(Constants.TAG_ID));
				tag.setTagName(resultSet.getString(Constants.TAG_NAME));
				tagList.add(tag);
			}

		} catch (SQLException e) {
			LOG.error("Finding tag list failed", e);
			throw new DAOException("Finding tag list failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getTagListStatement);
		}
		return tagList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.dao.imp.TagDao#getTagListByNewsId(java.lang.Long)
	 */
	public List<Tag> getTagListByNewsId(Long newsId) throws DAOException {
		LinkedList<Tag> tagList = new LinkedList<Tag>();
		Connection connection = getConnection();
		PreparedStatement getTagListStatement = null;
		ResultSet resultSet = null;
		try {
			getTagListStatement = openPrepStatement(connection, Constants.GET_TAG_LIST_BY_NEWS_ID);
			getTagListStatement.setLong(1, newsId);
			resultSet = getTagListStatement.executeQuery();
			while (resultSet.next()) {
				Tag tag = new Tag();
				tag.setId(resultSet.getLong(Constants.TAG_ID));
				tag.setTagName(resultSet.getString(Constants.TAG_NAME));
				tagList.add(tag);
			}

		} catch (SQLException e) {
			LOG.error("Finding tag list failed", e);
			throw new DAOException("Finding tag list failed", e);
		} finally {
			closeResultSet(resultSet);
			close(connection, getTagListStatement);
		}
		return tagList;
	}

	public boolean deleteTagById(Long tagId) throws DAOException {
		boolean result = false;
		Connection connection = getConnection();
		PreparedStatement deleteTagStatement = null;
		try {
			deleteTagStatement = openPrepStatement(connection, Constants.DEL_TAG_BY_ID);
			deleteTagStatement.setLong(1, tagId);
			deleteTagStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Delleting tag failed", e);
			throw new DAOException("Delleting tag failed", e);
		} finally {
			close(connection, deleteTagStatement);
		}

		return result;
	}
	
	public boolean unlinkTagAndNews(Long tagId) throws DAOException {
		boolean result = false;
		Connection connection = getConnection();
		PreparedStatement unlinkStatement = null;
		try {
			unlinkStatement = openPrepStatement(connection, Constants.DEL_NEWS_TAG_BY_ID);
			unlinkStatement.setLong(1, tagId);
			unlinkStatement.executeUpdate();
			result = true;
		} catch (SQLException e) {
			LOG.error("Unlinking tag failed", e);
			throw new DAOException("Unlinking tag failed", e);
		} finally {
			close(connection, unlinkStatement);
		}

		return result;
	}
}
