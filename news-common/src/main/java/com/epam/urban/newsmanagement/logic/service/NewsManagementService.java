package com.epam.urban.newsmanagement.logic.service;

import java.util.List;

import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.model.Filter;
import com.epam.urban.newsmanagement.model.News;
import com.epam.urban.newsmanagement.model.NewsVO;

public interface NewsManagementService {
	
	/**
	 * Delete news by news id list
	 * 
	 * @param {@code Long[]} newsId
	 * @throws LogicException
	 */
	public void delete(Long[] newsId) throws LogicException;	
	
	/**
	 * Get sorted news list by creation date and number of comments
	 * 
	 * @return {@code List<NewsVO>} sorted news list
	 * @throws LogicException
	 */
	public List<NewsVO> getSortedNewsList() throws LogicException;
	
	/**
	 * Add news with author and tags
	 * 
	 * @param {@code NewsVO} newsVO
	 * @throws LogicException
	 */
	public void add(NewsVO newsVO) throws LogicException;
	
	public void updateNews(News news, Filter filter) throws LogicException;
	
	public NewsVO getSingleNews(Long newsId) throws LogicException;

}
