package com.epam.urban.newsmanagement.model;

import java.util.List;

public class Filter {

	private List<Author> author;
	private List<Tag> tag;
	
	private Long checkedAuthor = null;
	private Long[] checkedTags = null;


	public Long getCheckedAuthor() {
		return checkedAuthor;
	}

	public void setCheckedAuthor(Long checkedAuthor) {
		this.checkedAuthor = checkedAuthor;
	}

	public Long[] getCheckedTags() {
		return checkedTags;
	}

	public void setCheckedTags(Long[] checkedTags) {
		this.checkedTags = checkedTags;
	}

	public List<Author> getAuthor() {
		return author;
	}

	public void setAuthor(List<Author> author) {
		this.author = author;
	}

	public List<Tag> getTag() {
		return tag;
	}

	public void setTag(List<Tag> tag) {
		this.tag = tag;
	}



}
