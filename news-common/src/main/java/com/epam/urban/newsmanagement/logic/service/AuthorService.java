package com.epam.urban.newsmanagement.logic.service;

import java.util.List;

import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.model.Author;

public interface AuthorService {

	/**
	 * Link author
	 * 
	 * @param {@code Author} author
	 * @return {@code true}, if successfully linked
	 * @throws LogicException
	 */
	public boolean linkAuthor(Author author) throws LogicException;

	/**
	 * Add author
	 * 
	 * @param {@code Author} author
	 * @return {@code true}, if successfully added
	 * @throws LogicException
	 */
	public boolean addAuthor(Author author) throws LogicException;

	/**
	 * Save author
	 * 
	 * @param {@code Author} author
	 * @return {@code true}, if successfully saved
	 * @throws LogicException
	 */
	public boolean save(Author author) throws LogicException;
	
	public boolean updateNewsAuthor(Long newsId, Long authorId) throws LogicException;

	/**
	 * Delete author
	 * 
	 * @param {@code Long[]} author id
	 * @return {@code true}, if successfully deleted
	 * @throws LogicException
	 */
	public boolean deleteAuthor(Long[] id) throws LogicException;

	/**
	 * Get single news author
	 * 
	 * @param {@code Long} id
	 * @return {@code String} single news author name
	 * @throws LogicException
	 */
	public String getNewsAuthor(Long id) throws LogicException;

	/**
	 * Get author list
	 * 
	 * @return {@code List<Author>} author list
	 * @throws LogicException
	 */
	public List<Author> getList() throws LogicException;

	/**
	 * Delete author by news id
	 * 
	 * @param {@code Long} news id
	 * @return {@code true}, if successfully deleted
	 * @throws LogicException
	 */
	public boolean deleteAuthorByNewsId(Long newsId) throws LogicException;
	
	public List<Author> getNotExpiredAuthorList() throws LogicException;
	
	public boolean expireAuthor(Long id) throws LogicException;

}