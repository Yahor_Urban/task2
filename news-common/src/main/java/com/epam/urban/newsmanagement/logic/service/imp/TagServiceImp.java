package com.epam.urban.newsmanagement.logic.service.imp;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.urban.newsmanagement.dao.TagDao;
import com.epam.urban.newsmanagement.exception.DAOException;
import com.epam.urban.newsmanagement.exception.LogicException;
import com.epam.urban.newsmanagement.logic.service.TagService;
import com.epam.urban.newsmanagement.model.Tag;

/**
 * tag service class
 * 
 * @author Urban Egor
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class TagServiceImp implements TagService {

	private static final Logger LOG = Logger.getLogger(TagServiceImp.class);
	
	@Autowired
	private TagDao tagDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.TagService#addTag(com.epam.urban .newsmanagement.model.Tag)
	 */
	public boolean addTag(Tag tag) throws LogicException {
		if (tag.getTagName() == null || tag.getTagName().isEmpty()) {
			LOG.error("Tag name is empty");
			throw new LogicException("Tag name is empty");
		}
		boolean rezult = false;
		try {
			if (tagDao.getTagId(tag.getTagName()) == 0) {
				tagDao.addTag(tag);
				rezult = true;
			} else {
				LOG.error("Tag is exist");
				throw new LogicException("Tag is exist");
			}
		} catch (DAOException e) {
			LOG.error("Adding tag failed", e);
			throw new LogicException("Adding tag failed", e);
		}

		return rezult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.TagService#linkTag(com.epam.urban .newsmanagement.model.Tag)
	 */
	public boolean linkTag(Tag tag) throws LogicException {
		if (tag.getTagName() == null || tag.getTagName().isEmpty()) {
			LOG.error("Tag name is empty");
			throw new LogicException("Tag name is empty");
		} else if (tag.getNewsId() == null) {
			LOG.error("Incorrect newsId");
			throw new LogicException("Incorrect newsId");
		}
		boolean rezult = false;
		try {
			if (addTag(tag)) {
				if (!tagDao.linkTag(tag.getNewsId(), tag.getId())) {
					LOG.error("Adding tag failed");
					throw new LogicException("Adding tag failed");
				}
			}
			rezult = true;
		} catch (DAOException e) {
			LOG.error("Adding tag failed", e);
			throw new LogicException("Adding tag failed", e);
		}

		return rezult;
	}
	
	public boolean linkTag(Long newsId, Long tagId) throws LogicException {
		if (newsId == null || tagId == null) {
			LOG.error("Incorrect data");
			throw new LogicException("Incorrect data");
		}
		try {
			tagDao.linkTag(newsId, tagId);
			return true;
		} catch (DAOException e) {
			LOG.error("Linking news tag failed", e);
			throw new LogicException("Linking news tag failed", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.TagService#save(com.epam. urban.newsmanagement.model.Tag)
	 */
	public boolean save(Tag tag) throws LogicException {
		if (tag.getId() == null || tag.getTagName() == null || tag.getTagName().isEmpty()) {
			LOG.error("Incorrect newsId");
			throw new LogicException("Incorrect newsId");
		}
		boolean rezult = false;
		try {
			tagDao.saveTag(tag);
			rezult = true;
		} catch (DAOException e) {
			LOG.error("Save tag failed");
			throw new LogicException("Save tag failed");
		}
		return rezult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.TagService#deleteTag(java.lang .Long)
	 */
	public boolean deleteTag(Long tagId) throws LogicException {
		if (tagId == null) {
			LOG.error("Tag id is null");
			throw new LogicException("Tag id is null");
		}
		try {
			tagDao.deleteTagById(tagId);
			tagDao.unlinkTagAndNews(tagId);
		} catch (DAOException e) {
			LOG.error("Deleting tag failed", e);
			throw new LogicException("Deleting tag failed", e);
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.TagService#deleteTagByNewId(java.lang .Long)
	 */
	public boolean deleteTagByNewId(Long newsId) throws LogicException {
		if (newsId == null) {
			LOG.error("News id is null");
			throw new LogicException("News id is null");
		}
		boolean result = false;
		try {
			tagDao.deleteTagByNewsId(newsId);
			result = true;
		} catch (DAOException e) {
			LOG.error("Deleting tag failed", e);
			throw new LogicException("Deleting tag failed", e);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.TagService#getTagList()
	 */
	public List<Tag> getTagList() throws LogicException {
		List<Tag> list = null;
		try {
			list = tagDao.getTagList();
		} catch (DAOException e) {
			LOG.error("Finding tag list failed");
			throw new LogicException("Finding tag list failed");
		}
		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.urban.newsmanagement.logic.service.TagService#getTagListByNewsId (java.lang.Long)
	 */
	public List<Tag> getTagListByNewsId(Long id) throws LogicException {
		List<Tag> list = null;
		try {
			list = tagDao.getTagListByNewsId(id);
		} catch (DAOException e) {
			LOG.error("Finding tag list by news id failed");
			throw new LogicException("Finding tag list by news id failed");
		}
		return list;
	}
}
